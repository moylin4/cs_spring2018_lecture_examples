﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX6
{
    class Program
    {
        static void Main(string[] args)
        {
            var dict = new Dictionary<int, string>();
            dict[3] = "foo";
            dict[5] = "bar";
            //dict[7] = null;

            //if (dict.ContainsKey(7))
            //{
            //    Console.WriteLine(dict[7]);
            //}
            //else
            //{
            //    Console.WriteLine("Does not contain key");
            //}

            if (dict.TryGetValue(7, out string value))
            {
                Console.WriteLine(value);
            }
            else
            {
                Console.WriteLine("Does not contain key");
            }
        }
    }
}
