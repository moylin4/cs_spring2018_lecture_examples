﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX3
{
    class Program
    {
        static void Main(string[] args)
        {
            string name = "Roger";

            // method 1
            foreach (char c in name)
            {
                Console.WriteLine(c);
            }
            Console.WriteLine();

            // method 2
            IEnumerator<char> itr = name.GetEnumerator();
            while (itr.MoveNext())
            {
                char c = itr.Current;
                Console.WriteLine(c);
            }
            Console.WriteLine();

        }
    }
}
