﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX4
{
    class Program
    {
        static void Main(string[] args)
        {
            var names = new Dictionary<string, string>();
            names["Geoffrey"] = "Gibson";
            names["Fred"] = "Flinstone";
            names["Unknown"] = null;

            foreach (var entry in names)
            {
                Console.WriteLine($"{entry.Key}: {entry.Value}");
            }
            Console.WriteLine();

            foreach (var entry in names.Keys)
            {
                Console.WriteLine(entry);
            }
            Console.WriteLine();

            foreach (var entry in names.Values)
            {
                Console.WriteLine(entry);
            }
            Console.WriteLine();

        }
    }
}
