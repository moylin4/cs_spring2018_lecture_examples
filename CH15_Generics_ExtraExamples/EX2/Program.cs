﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX2
{
    class Program
    {
        static void Main(string[] args)
        {
            for (; ; )
            {
                Console.Write("Name: ");
                string name = Console.ReadLine();
                Console.WriteLine(Hash1(name));
                Console.WriteLine(Hash2(name));
                Console.WriteLine(Hash3(name));
            }
        }

        static int Hash1(string input)
        {
            int sum = 0;
            foreach (char c in input)
            {
                sum += (int)c;
            }
            return sum;
        }

        static int Hash2(string input)
        {
            int sum = 0;
            for (int i = 0; i < input.Length; ++i)
            {
                sum = (sum + input[i] * (i + 1));
            }
            return sum;
        }

        static int Hash3(string input)
        {
            return input.GetHashCode();
        }
    }
}
