﻿namespace EX5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lstProducts = new System.Windows.Forms.ListBox();
            this.btnSortById = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSortByName = new System.Windows.Forms.Button();
            this.btnSortByCost = new System.Windows.Forms.Button();
            this.btnSortBySale = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lstProducts
            // 
            this.lstProducts.FormattingEnabled = true;
            this.lstProducts.ItemHeight = 20;
            this.lstProducts.Location = new System.Drawing.Point(40, 37);
            this.lstProducts.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lstProducts.Name = "lstProducts";
            this.lstProducts.Size = new System.Drawing.Size(416, 384);
            this.lstProducts.TabIndex = 0;
            // 
            // btnSortById
            // 
            this.btnSortById.Location = new System.Drawing.Point(40, 475);
            this.btnSortById.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSortById.Name = "btnSortById";
            this.btnSortById.Size = new System.Drawing.Size(112, 35);
            this.btnSortById.TabIndex = 1;
            this.btnSortById.Text = "Id";
            this.btnSortById.UseVisualStyleBackColor = true;
            this.btnSortById.Click += new System.EventHandler(this.btnSortById_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 434);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Sort By";
            // 
            // btnSortByName
            // 
            this.btnSortByName.Location = new System.Drawing.Point(164, 475);
            this.btnSortByName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSortByName.Name = "btnSortByName";
            this.btnSortByName.Size = new System.Drawing.Size(112, 35);
            this.btnSortByName.TabIndex = 3;
            this.btnSortByName.Text = "Name";
            this.btnSortByName.UseVisualStyleBackColor = true;
            this.btnSortByName.Click += new System.EventHandler(this.btnSortByName_Click);
            // 
            // btnSortByCost
            // 
            this.btnSortByCost.Location = new System.Drawing.Point(286, 475);
            this.btnSortByCost.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSortByCost.Name = "btnSortByCost";
            this.btnSortByCost.Size = new System.Drawing.Size(112, 35);
            this.btnSortByCost.TabIndex = 4;
            this.btnSortByCost.Text = "Cost";
            this.btnSortByCost.UseVisualStyleBackColor = true;
            this.btnSortByCost.Click += new System.EventHandler(this.btnSortByCost_Click);
            // 
            // btnSortBySale
            // 
            this.btnSortBySale.Location = new System.Drawing.Point(40, 518);
            this.btnSortBySale.Name = "btnSortBySale";
            this.btnSortBySale.Size = new System.Drawing.Size(112, 43);
            this.btnSortBySale.TabIndex = 5;
            this.btnSortBySale.Text = "Sale";
            this.btnSortBySale.UseVisualStyleBackColor = true;
            this.btnSortBySale.Click += new System.EventHandler(this.btnSortBySale_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(489, 605);
            this.Controls.Add(this.btnSortBySale);
            this.Controls.Add(this.btnSortByCost);
            this.Controls.Add(this.btnSortByName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSortById);
            this.Controls.Add(this.lstProducts);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstProducts;
        private System.Windows.Forms.Button btnSortById;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSortByName;
        private System.Windows.Forms.Button btnSortByCost;
        private System.Windows.Forms.Button btnSortBySale;
    }
}

