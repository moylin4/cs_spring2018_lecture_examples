﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace EX5
{
    public partial class Form1 : Form
    {
        private List<Product> _products;

        public Form1()
        {
            InitializeComponent();

            _products = new List<Product>();
            _products.Add(new Product
            {
                Id = 1, Name = "Kitten", Cost = 200
            });
            _products.Add(new Product
            {
                Id = 2, Name = "Ball of Yarn", Cost = 5
            });
            _products.Add(new Product
            {
                Id = 3, Name = "Box", Cost = 2
            });
            _products.Add(new SaleProduct
            {
                Id = 3,
                Name = "Box",
                Cost = 2,
                SalePercent = 0.5f
            });

            ShowProducts();
        }

        private void ShowProducts()
        {
            lstProducts.Items.Clear();
            foreach (Product p in _products)
            {
                lstProducts.Items.Add(p);
            }
        }

        private void btnSortById_Click(object sender, EventArgs e)
        {
            _products.Sort((x, y) => x.Id.CompareTo(y.Id));
            ShowProducts();
        }

        private void btnSortByName_Click(object sender, EventArgs e)
        {
            _products.Sort((x, y) => x.Name.CompareTo(y.Name));
            ShowProducts();
        }

        private void btnSortByCost_Click(object sender, EventArgs e)
        {
            _products.Sort((x, y) => x.Cost.CompareTo(y.Cost));
            ShowProducts();
        }

        private void btnSortBySale_Click(object sender, EventArgs e)
        {
            //_products.Sort((x, y) => {
            //    Armor a = x as Armor;
            //    Armor b = y as Armor;
            //    int c = (a != null ? a.DEF : 0);
            //    int d = (b != null ? b.DEF : 0);
            //    return -c.CompareTo(d);
            //});
            _products.Sort((x, y) =>
            {
                float c = (x as SaleProduct)?.SalePercent ?? 0;
                float d = (y as SaleProduct)?.SalePercent ?? 0;
                return -c.CompareTo(d);
            });
            ShowProducts();
        }
    }
}
