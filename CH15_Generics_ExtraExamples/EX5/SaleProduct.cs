﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX5
{
    public class SaleProduct : Product
    {
        public float SalePercent { get; set; }

        public override string ToString()
        {
            return $"{Id}: {Name} @ {Cost:C} each, {SalePercent:P} OFF";
        }
    }
}
