﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX5
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public float Cost { get; set; }

        public override string ToString()
        {
            return $"{Id}: {Name} @ {Cost:C} each";
        }
    }
}
