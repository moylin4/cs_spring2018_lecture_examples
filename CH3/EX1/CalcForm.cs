﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX1
{
    public partial class CalcForm : Form
    {
        public CalcForm()
        {
            InitializeComponent();
        }

        private void btnCalculateCost_Click(object sender, EventArgs e)
        {
            float price = Convert.ToSingle(txtPrice.Text);
            int quantity = Convert.ToInt32(txtQuantity.Text);
            float totalCost = price * quantity;
            lblTotalCost.Text = 
                string.Format("Total Cost: {0:C}", totalCost);
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            txtPrice.Text = "";
            txtQuantity.Text = "";
            lblTotalCost.Text = "";
        }
    }
}
