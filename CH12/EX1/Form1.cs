﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX1
{
  public partial class Form1 : Form
  {
    public Form1()
    {
      //label1.Text = "Hello";
      InitializeComponent();
      label1.Text = "Hello";
      label1.Font = new Font("Comic Sans MS", 20, FontStyle.Regular);
    }

    private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
      //label1.Text = "what?";
      //System.Diagnostics.Process.Start(@"https://insideranken.org");
      //System.Diagnostics.Process.Start(@"C:\Users\Paul\Desktop\hello.txt");

      //label1.ForeColor = Color.LimeGreen;
      //label1.ForeColor = Color.FromArgb(255, 0, 255, 0);
      //label1.ForeColor = Color.FromArgb(unchecked((int)0xFF0000));
      label1.ForeColor = Color.FromArgb(unchecked((int)0xFF48D1CC));
    }
  }
}
