﻿namespace EX2
{
  partial class StudentSelectForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.comboStudents = new System.Windows.Forms.ComboBox();
      this.lblId = new System.Windows.Forms.Label();
      this.lblFirstName = new System.Windows.Forms.Label();
      this.lblLastName = new System.Windows.Forms.Label();
      this.lblGpa = new System.Windows.Forms.Label();
      this.btnReset = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // comboStudents
      // 
      this.comboStudents.FormattingEnabled = true;
      this.comboStudents.Location = new System.Drawing.Point(12, 12);
      this.comboStudents.Name = "comboStudents";
      this.comboStudents.Size = new System.Drawing.Size(293, 28);
      this.comboStudents.TabIndex = 0;
      this.comboStudents.SelectedIndexChanged += new System.EventHandler(this.comboStudents_SelectedIndexChanged);
      // 
      // lblId
      // 
      this.lblId.AutoSize = true;
      this.lblId.Location = new System.Drawing.Point(12, 61);
      this.lblId.Name = "lblId";
      this.lblId.Size = new System.Drawing.Size(21, 20);
      this.lblId.TabIndex = 1;
      this.lblId.Text = "id";
      // 
      // lblFirstName
      // 
      this.lblFirstName.AutoSize = true;
      this.lblFirstName.Location = new System.Drawing.Point(12, 96);
      this.lblFirstName.Name = "lblFirstName";
      this.lblFirstName.Size = new System.Drawing.Size(35, 20);
      this.lblFirstName.TabIndex = 2;
      this.lblFirstName.Text = "first";
      // 
      // lblLastName
      // 
      this.lblLastName.AutoSize = true;
      this.lblLastName.Location = new System.Drawing.Point(12, 127);
      this.lblLastName.Name = "lblLastName";
      this.lblLastName.Size = new System.Drawing.Size(34, 20);
      this.lblLastName.TabIndex = 3;
      this.lblLastName.Text = "last";
      // 
      // lblGpa
      // 
      this.lblGpa.AutoSize = true;
      this.lblGpa.Location = new System.Drawing.Point(12, 165);
      this.lblGpa.Name = "lblGpa";
      this.lblGpa.Size = new System.Drawing.Size(36, 20);
      this.lblGpa.TabIndex = 4;
      this.lblGpa.Text = "gpa";
      // 
      // btnReset
      // 
      this.btnReset.Location = new System.Drawing.Point(12, 250);
      this.btnReset.Name = "btnReset";
      this.btnReset.Size = new System.Drawing.Size(108, 49);
      this.btnReset.TabIndex = 5;
      this.btnReset.Text = "Reset";
      this.btnReset.UseVisualStyleBackColor = true;
      this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(503, 437);
      this.Controls.Add(this.btnReset);
      this.Controls.Add(this.lblGpa);
      this.Controls.Add(this.lblLastName);
      this.Controls.Add(this.lblFirstName);
      this.Controls.Add(this.lblId);
      this.Controls.Add(this.comboStudents);
      this.Name = "Form1";
      this.Text = "Form1";
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ComboBox comboStudents;
    private System.Windows.Forms.Label lblId;
    private System.Windows.Forms.Label lblFirstName;
    private System.Windows.Forms.Label lblLastName;
    private System.Windows.Forms.Label lblGpa;
    private System.Windows.Forms.Button btnReset;
  }
}

