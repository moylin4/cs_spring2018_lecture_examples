﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX2
{
  public partial class StudentSelectForm : Form
  {
    public StudentSelectForm()
    {
      InitializeComponent();

      object[] items = new object[]
      {
        new Student { Id = 123, FirstName = "Foo", LastName = "Bar", Gpa = 3.0f },
        new Student { Id = 456, FirstName = "Swoop", LastName = "Swoop", Gpa = 3.2f },
        new Student { Id = 456, FirstName = "Jeff", LastName = "Black", Gpa = 2.5f },
      };
      comboStudents.Items.Clear();
      comboStudents.Items.AddRange(items);
    }

    private void comboStudents_SelectedIndexChanged(object sender, EventArgs e)
    {
      Student student = comboStudents.SelectedItem as Student;

      if (student != null)
      {
        lblId.Text = string.Format("ID: {0}", student.Id);
        lblFirstName.Text = string.Format("First Name: {0}", student.FirstName);
        lblLastName.Text = string.Format("Last Name: {0}", student.LastName);
        lblGpa.Text = string.Format("GPA: {0:F2}", student.Gpa);
      }
      else
      {
        lblId.Text = "";
        lblFirstName.Text = "";
        lblLastName.Text = "";
        lblGpa.Text = "";
      }
    }

    private void btnReset_Click(object sender, EventArgs e)
    {
      comboStudents.SelectedItem = null;
    }
  }
}
