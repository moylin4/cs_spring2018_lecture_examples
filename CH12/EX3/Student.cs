﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX3
{
  public class Student
  {
    private int _id;
    private string _firstName;
    private string _lastName;
    private float _gpa;

    public int Id
    {
      get { return _id; }
      set { _id = value; }
    }
    public string FirstName
    {
      get { return _firstName; }
      set { _firstName = value; }
    }
    public string LastName
    {
      get { return _lastName; }
      set { _lastName = value; }
    }
    public float Gpa
    {
      get { return _gpa; }
      set { _gpa = value; }
    }

    public override string ToString()
    {
      return _firstName + " " + _lastName;
    }

  }
}
