﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX3
{
  public partial class ReportForm : Form
  {
    private Student[] _students;

    public ReportForm(Student[] students)
    {
      InitializeComponent();
      _students = students;

      this.SuspendLayout();

      for (int i = 0; i < students.Length; ++i)
      {
        Label label = new Label();
        label.Name = "lblStudent" + i;
        label.Text = string.Format(
          "ID: {0}\r\nFirst Name: {1}\r\nLast Name: {2}\r\nGPA: {3:F2}",
          students[i].Id,
          students[i].FirstName,
          students[i].LastName,
          students[i].Gpa
        );
        label.Location = new Point(10, 10 + 70 * i);
        label.AutoSize = true;
        this.Controls.Add(label);
      }

      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
