﻿namespace EX3
{
  partial class StudentListForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.listStudents = new System.Windows.Forms.ListBox();
      this.btnReset = new System.Windows.Forms.Button();
      this.btnShowReport = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // listStudents
      // 
      this.listStudents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
      this.listStudents.FormattingEnabled = true;
      this.listStudents.ItemHeight = 20;
      this.listStudents.Location = new System.Drawing.Point(12, 12);
      this.listStudents.Name = "listStudents";
      this.listStudents.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
      this.listStudents.Size = new System.Drawing.Size(278, 264);
      this.listStudents.TabIndex = 0;
      // 
      // btnReset
      // 
      this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnReset.Location = new System.Drawing.Point(174, 290);
      this.btnReset.Name = "btnReset";
      this.btnReset.Size = new System.Drawing.Size(116, 55);
      this.btnReset.TabIndex = 6;
      this.btnReset.Text = "Reset";
      this.btnReset.UseVisualStyleBackColor = true;
      this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
      // 
      // btnShowReport
      // 
      this.btnShowReport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.btnShowReport.Location = new System.Drawing.Point(12, 290);
      this.btnShowReport.Name = "btnShowReport";
      this.btnShowReport.Size = new System.Drawing.Size(144, 55);
      this.btnShowReport.TabIndex = 7;
      this.btnShowReport.Text = "Show Report";
      this.btnShowReport.UseVisualStyleBackColor = true;
      this.btnShowReport.Click += new System.EventHandler(this.btnShowReport_Click);
      // 
      // StudentListForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(323, 357);
      this.Controls.Add(this.btnShowReport);
      this.Controls.Add(this.btnReset);
      this.Controls.Add(this.listStudents);
      this.MinimumSize = new System.Drawing.Size(345, 200);
      this.Name = "StudentListForm";
      this.Text = "Form1";
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ListBox listStudents;
    private System.Windows.Forms.Button btnReset;
    private System.Windows.Forms.Button btnShowReport;
  }
}

