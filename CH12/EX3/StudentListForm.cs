﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX3
{
  public partial class StudentListForm : Form
  {
    public StudentListForm()
    {
      InitializeComponent();

      object[] items = new object[]
      {
        new Student { Id = 123, FirstName = "Foo", LastName = "Bar", Gpa = 3.0f },
        new Student { Id = 456, FirstName = "Swoop", LastName = "Swoop", Gpa = 3.2f },
        new Student { Id = 456, FirstName = "Jeff", LastName = "Black", Gpa = 2.5f },
      };
      listStudents.Items.Clear();
      listStudents.Items.AddRange(items);
    }

    private void btnShowReport_Click(object sender, EventArgs e)
    {
      var selected = listStudents.SelectedItems;
      var students = new Student[selected.Count];

      for (int i = 0; i < selected.Count; ++i)
      {
        students[i] = selected[i] as Student;
      }

      var report = new ReportForm(students);
      report.ShowDialog();
    }

    private void btnReset_Click(object sender, EventArgs e)
    {
      listStudents.SelectedItems.Clear();
    }
  }
}
