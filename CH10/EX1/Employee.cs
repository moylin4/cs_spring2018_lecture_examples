﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX1
{
    public class Employee
    {
        protected int _id;
        protected double _salary;

        public Employee(int id, double salary)
        {
            _id = id;
            _salary = salary;
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public double Salary
        {
            get { return _salary; }
            set { _salary = value; }
        }

        public virtual string GetGreeting()
        {
            return string.Format("Hello. I am employee #{0}", _id);
        }

        public override bool Equals(object obj)
        {
            if (obj == null) { return false; }
            if (object.ReferenceEquals(this, obj)) { return true; }
            if (this.GetType() != obj.GetType()) { return false; }

            Employee other = (Employee)obj;
            return this._id == other._id;

            //return this._id == other._id && 
            //       this._salary == other._salary;
        }

        public override int GetHashCode()
        {
            return _id.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format(
                "{0}: id = {1}, salary = {2:C}",
                GetType().Name, _id, _salary
            );
        }
    }
}
