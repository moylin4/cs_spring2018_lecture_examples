﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX1
{
    class Program
    {
        //static Employee clerk;
        //static CommissionEmployee salesperson;

        static void Main(string[] args)
        {
            Employee clerk = 
                new Employee(123, 30000.00);
            CommissionEmployee salesperson = 
                new CommissionEmployee(234, 20000.00, 0.07);

            Console.WriteLine(clerk.Equals(salesperson));
            Console.WriteLine(salesperson.Equals(clerk));

            Console.WriteLine(clerk.ToString());
            Console.WriteLine(salesperson.ToString());

            //Console.WriteLine();
            //Console.WriteLine(clerk.GetGreeting());
            //Console.WriteLine(
            //    "Clerk #{0} salary: {1:C} per year",
            //    clerk.Id, clerk.Salary
            //);
            //Console.WriteLine();
            //Console.WriteLine(salesperson.GetGreeting());
            //Console.WriteLine(
            //    "Salesperson #{0} salary: {1:C} per year",
            //    salesperson.Id, salesperson.Salary
            //);
            //Console.WriteLine(
            //    "... plus {0:P} commission on all sales",
            //    salesperson.CommissionRate
            //);
        }
    }
}
