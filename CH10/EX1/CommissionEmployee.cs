﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX1
{
    public class CommissionEmployee : Employee
    {
        protected double _commissionRate;

        public CommissionEmployee(
            int id, 
            double salary, 
            double commissionRate) : base(id, salary)
        {
            //_id = id;
            //_salary = salary;
            _commissionRate = commissionRate;
        }

        public double CommissionRate
        {
            get { return _commissionRate; }
            set { _commissionRate = value; }
        }

        public override string GetGreeting()
        {
            string greeting = base.GetGreeting();
            greeting += "\nI work for commission";
            return greeting;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) { return false; }
            if (object.ReferenceEquals(this, obj)) { return true; }
            if (this.GetType() != obj.GetType()) { return false; }

            CommissionEmployee other = (CommissionEmployee)obj;
            return this._id == other.Id &&
                   this._salary == other.Salary &&
                   this._commissionRate == other._commissionRate;
        }

        public override int GetHashCode()
        {
            return _id.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format(
                "{0}, commission = {1:P}",
                base.ToString(), _commissionRate
            );
        }
    }
}
