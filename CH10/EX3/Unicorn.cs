﻿using System.Drawing;

namespace EX3
{
    public class Unicorn
    {
        protected string _name;
        protected Image _image;
        protected Weapon _wand;
        protected Armor _robe;
        protected Armor _hat;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public Image Image
        {
            get { return _image; }
            set { _image = value; }
        }

        public Weapon Wand
        {
            get { return _wand; }
            set { _wand = value; }
        }

        public Armor Robe
        {
            get { return _robe; }
            set { _robe = value; }
        }

        public Armor Hat
        {
            get { return _hat; }
            set { _hat = value; }
        }
    }
}
