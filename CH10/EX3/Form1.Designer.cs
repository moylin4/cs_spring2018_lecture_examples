﻿namespace EX3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.unicornPanel = new System.Windows.Forms.Panel();
            this.lblRobe = new System.Windows.Forms.Label();
            this.lblUnicornName = new System.Windows.Forms.Label();
            this.lblWand = new System.Windows.Forms.Label();
            this.lblHat = new System.Windows.Forms.Label();
            this.hatPicture = new System.Windows.Forms.PictureBox();
            this.wandPicture = new System.Windows.Forms.PictureBox();
            this.robePicture = new System.Windows.Forms.PictureBox();
            this.btnRespawn = new System.Windows.Forms.Button();
            this.unicornPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hatPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wandPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.robePicture)).BeginInit();
            this.SuspendLayout();
            // 
            // unicornPanel
            // 
            this.unicornPanel.BackColor = System.Drawing.Color.Transparent;
            this.unicornPanel.BackgroundImage = global::EX3.Properties.Resources.unicorn;
            this.unicornPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.unicornPanel.Controls.Add(this.lblRobe);
            this.unicornPanel.Controls.Add(this.lblUnicornName);
            this.unicornPanel.Controls.Add(this.lblWand);
            this.unicornPanel.Controls.Add(this.lblHat);
            this.unicornPanel.Controls.Add(this.hatPicture);
            this.unicornPanel.Controls.Add(this.wandPicture);
            this.unicornPanel.Controls.Add(this.robePicture);
            this.unicornPanel.Location = new System.Drawing.Point(12, 12);
            this.unicornPanel.Name = "unicornPanel";
            this.unicornPanel.Size = new System.Drawing.Size(1278, 814);
            this.unicornPanel.TabIndex = 4;
            // 
            // lblRobe
            // 
            this.lblRobe.Font = new System.Drawing.Font("Comic Sans MS", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRobe.ForeColor = System.Drawing.Color.Violet;
            this.lblRobe.Location = new System.Drawing.Point(471, 574);
            this.lblRobe.Name = "lblRobe";
            this.lblRobe.Size = new System.Drawing.Size(351, 140);
            this.lblRobe.TabIndex = 6;
            this.lblRobe.Text = "robe";
            this.lblRobe.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblUnicornName
            // 
            this.lblUnicornName.Font = new System.Drawing.Font("Comic Sans MS", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnicornName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lblUnicornName.Location = new System.Drawing.Point(723, 220);
            this.lblUnicornName.Name = "lblUnicornName";
            this.lblUnicornName.Size = new System.Drawing.Size(453, 135);
            this.lblUnicornName.TabIndex = 7;
            this.lblUnicornName.Text = "name";
            // 
            // lblWand
            // 
            this.lblWand.Font = new System.Drawing.Font("Comic Sans MS", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWand.ForeColor = System.Drawing.Color.Violet;
            this.lblWand.Location = new System.Drawing.Point(211, 488);
            this.lblWand.Name = "lblWand";
            this.lblWand.Size = new System.Drawing.Size(223, 181);
            this.lblWand.TabIndex = 5;
            this.lblWand.Text = "wand";
            this.lblWand.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblHat
            // 
            this.lblHat.AutoSize = true;
            this.lblHat.Font = new System.Drawing.Font("Comic Sans MS", 26F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lblHat.Location = new System.Drawing.Point(659, 63);
            this.lblHat.Name = "lblHat";
            this.lblHat.Size = new System.Drawing.Size(114, 72);
            this.lblHat.TabIndex = 4;
            this.lblHat.Text = "hat";
            // 
            // hatPicture
            // 
            this.hatPicture.BackColor = System.Drawing.Color.Transparent;
            this.hatPicture.Image = global::EX3.Properties.Resources.x;
            this.hatPicture.Location = new System.Drawing.Point(422, 32);
            this.hatPicture.Name = "hatPicture";
            this.hatPicture.Size = new System.Drawing.Size(221, 191);
            this.hatPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.hatPicture.TabIndex = 2;
            this.hatPicture.TabStop = false;
            // 
            // wandPicture
            // 
            this.wandPicture.BackColor = System.Drawing.Color.Transparent;
            this.wandPicture.Image = global::EX3.Properties.Resources.x;
            this.wandPicture.Location = new System.Drawing.Point(254, 358);
            this.wandPicture.Name = "wandPicture";
            this.wandPicture.Size = new System.Drawing.Size(147, 127);
            this.wandPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.wandPicture.TabIndex = 3;
            this.wandPicture.TabStop = false;
            this.wandPicture.Click += new System.EventHandler(this.wandPicture_Click);
            // 
            // robePicture
            // 
            this.robePicture.BackColor = System.Drawing.Color.Transparent;
            this.robePicture.Image = global::EX3.Properties.Resources.x;
            this.robePicture.Location = new System.Drawing.Point(517, 358);
            this.robePicture.Name = "robePicture";
            this.robePicture.Size = new System.Drawing.Size(221, 191);
            this.robePicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.robePicture.TabIndex = 1;
            this.robePicture.TabStop = false;
            // 
            // btnRespawn
            // 
            this.btnRespawn.Font = new System.Drawing.Font("Comic Sans MS", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRespawn.Location = new System.Drawing.Point(1178, 558);
            this.btnRespawn.Name = "btnRespawn";
            this.btnRespawn.Size = new System.Drawing.Size(226, 111);
            this.btnRespawn.TabIndex = 8;
            this.btnRespawn.Text = "Respawn";
            this.btnRespawn.UseVisualStyleBackColor = true;
            this.btnRespawn.Click += new System.EventHandler(this.btnRespawn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1524, 849);
            this.Controls.Add(this.btnRespawn);
            this.Controls.Add(this.unicornPanel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.unicornPanel.ResumeLayout(false);
            this.unicornPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.hatPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wandPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.robePicture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox robePicture;
        private System.Windows.Forms.PictureBox hatPicture;
        private System.Windows.Forms.PictureBox wandPicture;
        private System.Windows.Forms.Panel unicornPanel;
        private System.Windows.Forms.Label lblRobe;
        private System.Windows.Forms.Label lblWand;
        private System.Windows.Forms.Label lblHat;
        private System.Windows.Forms.Label lblUnicornName;
        private System.Windows.Forms.Button btnRespawn;
    }
}

