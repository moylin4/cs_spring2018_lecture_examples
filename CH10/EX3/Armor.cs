﻿using System.Drawing;

namespace EX3
{
    public class Armor : Item
    {
        protected int _defense;

        public Armor(
            string name,
            Image image,
            int defense) : base(name, image)
        {
            _defense = defense;
        }

        public int Defense
        {
            get { return _defense; }
        }
    }
}
