﻿using System.Drawing;

namespace EX3
{
    public abstract class Item
    {
        protected string _name;
        protected Image _image;

        public Item(string name, Image image)
        {
            _name = name;
            _image = image;
        }

        public string Name
        {
            get { return _name; }
        }

        public Image Image
        {
            get { return _image; }
        }
    }
}
