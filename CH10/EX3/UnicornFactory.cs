﻿using System;
using System.Drawing;

namespace EX3
{
    public class UnicornFactory
    {
        protected Random _rand = new Random();

        private string[] _unicornNames = new string[]
        {
            "Fluffy", "Sparkles", "Bunny"
        };
        private Image[] _unicornImages = new Image[]
        {
            EX3.Properties.Resources.unicorn,
            //EX3.Properties.Resources.unicorn2,
            //EX3.Properties.Resources.unicorn3,
        };
        private string[] _wandNames = new string[]
        {
            "Mr. Pickles", "Mr. Sparkles", "Mr. Pumpernickle"
        };
        private Image[] _wandImages = new Image[]
        {
            EX3.Properties.Resources.wand,
            EX3.Properties.Resources.wand2,
            EX3.Properties.Resources.wand3,
        };
        private string[] _robeNames = new string[]
        {
            "Robe of Invisibility",
            "Robe of Sunshine",
            "Robe of Friendship"
        };
        private Image[] _robeImages = new Image[]
        {
            EX3.Properties.Resources.robe,
            EX3.Properties.Resources.robe2,
            EX3.Properties.Resources.robe3,
        };
        private string[] _hatNames = new string[]
        {
            "Happy Hat",
            "Lucky Hat",
            "Frosty's Hat"
        };
        private Image[] _hatImages = new Image[]
        {
            EX3.Properties.Resources.hat,
            EX3.Properties.Resources.hat2,
            EX3.Properties.Resources.hat3,
        };

        private string RandomizeName(string[] names)
        {
            return names[_rand.Next(0, names.Length)];
        }

        private Image RandomizeImage(Image[] images)
        {
            return images[_rand.Next(0, images.Length)];
        }

        public Unicorn Create()
        {
            //string name = "Unicorn";
            //Image image = EX3.Properties.Resources.unicorn;
            string name = RandomizeName(_unicornNames);
            Image image = RandomizeImage(_unicornImages);

            Weapon wand = new Weapon(
                RandomizeName(_wandNames),
                RandomizeImage(_wandImages),
                _rand.Next(1, 100)
            );
            Armor robe = new Armor(
                RandomizeName(_robeNames),
                RandomizeImage(_robeImages),
                _rand.Next(1, 10)
            );
            Armor hat = new Armor(
                RandomizeName(_hatNames),
                RandomizeImage(_hatImages),
                _rand.Next(1, 10)
            );

            return new Unicorn()
            {
                Name = name,
                Image = image,
                Wand = wand,
                Robe = robe,
                Hat = hat
            };
        }
    }
}
