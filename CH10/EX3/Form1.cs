﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX3
{
    public partial class Form1 : Form
    {
        private UnicornFactory _factory;
        private Unicorn _unicorn;

        public Form1()
        {
            InitializeComponent();

            // Create a unicorn
            _factory = new UnicornFactory();
            _unicorn = _factory.Create();

            // Show the unicorn
            ShowUnicorn(_unicorn);

            Item item = new Weapon("name", null, 1);
        }

        private void btnRespawn_Click(object sender, EventArgs e)
        {
            _unicorn = _factory.Create();
            ShowUnicorn(_unicorn);
        }

        private void ShowUnicorn(Unicorn u)
        {
            // Show the images
            unicornPanel.BackgroundImage = u.Image;
            wandPicture.Image = u.Wand.Image;
            robePicture.Image = u.Robe.Image;
            hatPicture.Image = u.Hat.Image;

            // Show the names
            lblUnicornName.Text = u.Name;
            lblWand.Text = string.Format("{0} ({1:+0})", u.Wand.Name, u.Wand.Attack);
            lblRobe.Text = string.Format("{0} ({1:+0})", u.Robe.Name, u.Robe.Defense);
            lblHat.Text = string.Format("{0} ({1:+0})", u.Hat.Name, u.Hat.Defense);
        }

        private void wandPicture_Click(object sender, EventArgs e)
        {
            ExplodeForm form = new ExplodeForm();
            form.Show();
            //form.ShowDialog();

            SoundPlayer sound = new SoundPlayer(
                EX3.Properties.Resources.explosion
            );
            sound.Play();

            wandPicture.Location =
                new Point(wandPicture.Location.X, wandPicture.Location.Y + 20);
        }
    }
}
