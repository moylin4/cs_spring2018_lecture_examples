﻿using System.Drawing;

namespace EX3
{
    public class Weapon : Item
    {
        protected int _attack;

        public Weapon(
            string name,
            Image image,
            int attack) : base(name, image)
        {
            _attack = attack;
        }

        public int Attack
        {
            get { return _attack; }
        }
    }
}
