﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX4
{
    public static class ItemUtils
    {
        public static string GetName(this ItemType type)
        {
            switch (type)
            {
                case ItemType.VENDER_TRASH: return "Garbage";
                case ItemType.WEAPON: return "Weapon";
                case ItemType.ARMOR: return "Armor";
                default: return "MISSINGNO";
            }
        }
    }
}
