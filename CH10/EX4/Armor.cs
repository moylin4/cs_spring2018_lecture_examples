﻿using System.Drawing;

namespace EX4
{
    public class Armor : Item
    {
        protected int _defense;

        public Armor(
            int id, 
            string name, 
            double weight,
            int defense) : base(id, name, weight)
        {
            _defense = defense;
        }

        public int Defense
        {
            get { return _defense; }
        }

        public override string ToString()
        {
            return string.Format(
                "{0} ({1:+0} DEF) [{2:0.0} kg]", 
                _name, _defense, _weight
            );
        }
    }
}
