﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX4
{
    public class SortItemsByWeight : IComparer<Item>
    {
        public int Compare(Item x, Item y)
        {
            return x.Weight.CompareTo(y.Weight);
        }
    }
}
