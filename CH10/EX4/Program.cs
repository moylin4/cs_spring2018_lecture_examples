﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX4
{
    class Program
    {
        static void Main(string[] args)
        {
            //string name = ItemUtils.GetName(ItemType.VENDER_TRASH);
            //string name = ItemType.VENDER_TRASH.GetName();
            //Console.WriteLine(name);
            //Console.WriteLine();

            Item[] items = new Item[]
            {
                new Weapon(1, "Mr. Pickles", 5.3, 3),
                new Weapon(2, "Mr. Sparkles", 4.5, 4),
                new Armor(3, "Robe of Darkness", 3.4, 5),
                new Weapon(4, "Mr. Pumpernickle", 7.1, 6),
                new Armor(5, "Robe of Light", 8.0, 5),
                new Weapon(6, "Mr. Pickles", 5.3, 10),
                new Weapon(7, "Mr. Sparkles", 4.5, 20),
                new Weapon(8, "Mr. Pumpernickle", 7.1, 30),
            };

            Array.Sort(items);

            double totalWeight = 0.0;
            int totalAttack = 0;
            int totalDefense = 0;

            foreach (Item i in items)
            {
                Console.WriteLine(i.ToString());
                totalWeight += i.Weight;
                if (i is Weapon)
                {
                    totalAttack += ((Weapon)i).Attack;
                }
                if (i is Armor)
                {
                    totalDefense += ((Armor)i).Defense;
                }
            }

            Console.WriteLine();
            Console.WriteLine("weight: {0:0.0} kg", totalWeight);
            Console.WriteLine("attack: {0:0.0} ATK", totalAttack);
            Console.WriteLine("defense: {0:0.0} DEF", totalDefense);

            //for (int index = 0; index < items.Length; ++index)
            //{
            //    Item i = items[index];
            //    Console.WriteLine(i.ToString());
            //}
        }
    }
}
