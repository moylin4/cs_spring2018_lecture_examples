﻿using System.Drawing;

namespace EX4
{
    public class Weapon : Item
    {
        protected int _attack;

        public Weapon(
            int id,
            string name,
            double weight,
            int attack) : base(id, name, weight)
        {
            _attack = attack;
        }

        public int Attack
        {
            get { return _attack; }
        }

        public override string ToString()
        {
            return string.Format(
                "{0} ({1:+0} ATK) [{2:0.0} kg]",
                _name, _attack, _weight
            );
        }
    }
}
