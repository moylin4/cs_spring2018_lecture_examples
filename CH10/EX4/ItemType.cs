﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX4
{
    public enum ItemType
    {
        VENDER_TRASH = 0,
        ARMOR = 1,
        WEAPON = 2
    }
}
