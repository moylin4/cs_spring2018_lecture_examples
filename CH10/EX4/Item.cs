﻿using System;
using System.Drawing;

namespace EX4
{
    public abstract class Item : IComparable
    {
        protected int _id;
        protected string _name;
        protected double _weight; 

        public Item(int id, string name, double weight)
        {
            _id = id;
            _name = name;
            _weight = weight;
        }

        public int Id
        {
            get { return _id; }
        }

        public string Name
        {
            get { return _name; }
        }

        public double Weight
        {
            get { return _weight; }
        }

        public int CompareTo(object obj)
        {
            Item a = this;
            Item b = (Item)obj;

            int weightCompare = -a.Weight.CompareTo(b.Weight);
            return weightCompare;

            /*
            // Sort by type
            int typeCompare = a.GetType().Name.CompareTo(b.GetType().Name);
            if (typeCompare != 0) { return typeCompare; }

            // Then by name ascending
            int nameCompare = a.Name.CompareTo(b.Name);
            if (nameCompare != 0) { return nameCompare; }

            // Then by id ascending
            int idCompare = a.Id.CompareTo(b.Id);
            return idCompare;
            */
        }
    }
}
