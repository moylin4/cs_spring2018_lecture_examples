﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX1
{
    class Program
    {
        //static int Max(int[] arr)
        //{
        //    int max = arr[0];
        //    foreach (int x in arr)
        //    {
        //        if (x > max) { max = x; }
        //    }
        //    return max;
        //}

        //static float Max(float[] arr)
        //{
        //    float max = arr[0];
        //    foreach (float x in arr)
        //    {
        //        if (x > max) { max = x; }
        //    }
        //    return max;
        //}

        static T Max<T>(T[] arr) where T : IComparable
        {
            T max = arr[0];
            foreach (T x in arr)
            {
                if (x.CompareTo(max) > 0) { max = x; }
            }
            return max;
        }

        static void Main(string[] args)
        {
            int i = Max<int>(new int[] { 3, 7, 5 });
            Console.WriteLine(i);
            float f = Max<float>(new float[] { 3.5f, 7.2f, 5.3f });
            Console.WriteLine(f);
        }
    }
}
