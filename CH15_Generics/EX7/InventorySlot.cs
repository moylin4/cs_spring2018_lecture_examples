﻿namespace EX7
{
    public enum InventorySlot
    {
        NONE, HELMET, VEST, WEAPON
    }
}
