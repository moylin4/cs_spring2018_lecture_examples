﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX7
{
    class Program
    {
        static void Main(string[] args)
        {
            Bag bag = new Bag("Red Bag", 20);
            bag.Add(new Item
            {
                Name = "Sword of Glory",
                Description = "Awsome amazing sword",
                Slot = InventorySlot.WEAPON,
                Weight = 8
            });
            bag.Add(new Item
            {
                Name = "Pork and beans",
                Description = "Booty Tootie",
                Slot = InventorySlot.NONE,
                Weight = .25f
            });
            bag.Add(new Item
            {
                Name = "Mithril Chestplate",
                Description = "Tank all the hits",
                Slot = InventorySlot.VEST,
                Weight = 1
            });

            Dictionary<InventorySlot, Item> equipped = new Dictionary<InventorySlot, Item>();
            equipped[InventorySlot.WEAPON] = new Item
            {
                Name = "Hand of Doom",
                Description = "deathly ham",
                Slot = InventorySlot.WEAPON,
                Weight = 2
            };

            equipped[InventorySlot.VEST] = bag[2];
            bag.Remove(bag[2]);

            Console.WriteLine($"Weight {bag.TotalWeight}/{bag.MaxWeight} lbs");
            foreach (Item item in bag)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine();
            Console.WriteLine("Hand: {0}", equipped[InventorySlot.WEAPON]);
            Console.WriteLine("Vest: {0}", equipped[InventorySlot.VEST]);
        }
    }
}
