﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX7
{
    public class Bag : IEnumerable<Item>
    {
        protected string _name;
        protected List<Item> _items;
        protected float _totalWeight;
        protected float _maxWeight;

        public Bag(string name, float maxWeight)
        {
            _name = name;
            _items = new List<Item>();
            _totalWeight = 0;
            _maxWeight = maxWeight;
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public float TotalWeight
        {
            get { return _totalWeight; }
        }

        public float MaxWeight
        {
            get { return _maxWeight; }
        }

        private int Count
        {
            get { return _items.Count; }
        }

        public void Add(Item item)
        {
            if (item == null) { throw new NullReferenceException(); }

            float itemWeight = item.Weight;
            if (itemWeight + _totalWeight > _maxWeight)
            {
                throw new ArgumentException("There is no room");
            }
            else
            {
                _items.Add(item);
                _totalWeight += itemWeight;
            }
        }

        public Item Remove(Item item)
        {
            if (item == null) { throw new NullReferenceException(); }

            int index = _items.IndexOf(item);
            if (index >= 0)
            {
                Item removedItem = _items[index];
                _items.RemoveAt(index);
                _totalWeight -= removedItem.Weight;
                return removedItem;
            }
            else
            {
                return null;
            }
        }

        public Item this[int index]
        {
            get { return _items[index]; }
        }

        public IEnumerator<Item> GetEnumerator()
        {
            return _items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _items.GetEnumerator();
        }
    }
}
