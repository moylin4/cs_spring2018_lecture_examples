﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX7
{
    public class Item
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public InventorySlot Slot { get; set; }
        public float Weight { get; set; }

        public Item()
        {
            Id = Guid.NewGuid();
        }

        public override bool Equals(object obj)
        {
            Item other = obj as Item;
            if (other == null) { return false; }
            return this.Id == other.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
