﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX8
{
    class Program
    {
        static void Main(string[] args)
        {
            var students = new Dictionary<string, Student>();
            students["Greg"] = new Student("Greg", "Barns", 3.0);
            students["George"] = new Student("George", "Bailey", 3.2);

            Console.WriteLine("Name: ");
            string name = Console.ReadLine();
            Console.WriteLine(students[name]);

        }
    }
}
