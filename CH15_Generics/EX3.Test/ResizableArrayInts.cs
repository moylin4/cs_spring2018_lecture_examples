﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EX3.Test
{
    [TestClass]
    public class ResizableArrayInts
    {
        [TestMethod]
        public void EmptyArray_CountIsZero()
        {
            var array = new ResizableArray<int>(5);

            Assert.AreEqual(0, array.Count);
        }

        [TestMethod]
        public void EmptyArray_CannotAccessFirst()
        {
            var array = new ResizableArray<int>(5);

            Assert.ThrowsException<IndexOutOfRangeException>(
                () => array[0]
            );
        }

        [TestMethod]
        public void EmptyArray_CannotAccessNegativeOne()
        {
            var array = new ResizableArray<int>(5);

            Assert.ThrowsException<IndexOutOfRangeException>(
                () => array[-1]
            );
        }

        [TestMethod]
        public void Size1_CountIs1()
        {
            var array = new ResizableArray<int>(5);
            array.Add(7);

            Assert.AreEqual(1, array.Count);
        }

        [TestMethod]
        public void Size1_CanAccessFirst()
        {
            var array = new ResizableArray<int>(5);
            array.Add(7);
            
            Assert.AreEqual(7, array[0]);
        }

        [TestMethod]
        public void Size1_CannotAccessSecond()
        {
            var array = new ResizableArray<int>(5);
            array.Add(7);

            Assert.ThrowsException<IndexOutOfRangeException>(
                () => array[1]
            );
        }

        [TestMethod]
        public void Size1_CannotAccessNegativeOne()
        {
            var array = new ResizableArray<int>(5);
            array.Add(7);

            Assert.ThrowsException<IndexOutOfRangeException>(
                () => array[-1]
            );
        }

        [TestMethod]
        public void ResizesWhenNeeded()
        {
            var array = new ResizableArray<int>(2);
            array.Add(7);
            array.Add(9);
            array.Add(3);
            array.Add(11);

            Assert.AreEqual(4, array.Count);
            Assert.AreEqual(7, array[0]);
            Assert.AreEqual(9, array[1]);
            Assert.AreEqual(3, array[2]);
            Assert.AreEqual(11, array[3]);
        }
    }
}
