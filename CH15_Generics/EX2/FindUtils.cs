﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX2
{
    public static class FindUtils
    {
        public static int FindGeneric<T>(T[] arr, T search)
        {
            for (int index = 0; index < arr.Length; ++index)
            {
                if (arr[index].Equals(search))
                {
                    return index;
                }
            }
            return -1;
        }

        public delegate bool FindClause<T>(T value);

        public static T FindElement<T>(this T[] arr, FindClause<T> clause)
        {
            for (int index = 0; index < arr.Length; ++index)
            {
                if (clause(arr[index]))
                {
                    return arr[index];
                }
            }
            return default(T);
        }
    }
}
