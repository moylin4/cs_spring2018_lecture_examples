﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX2
{
    class Program
    {
        //static bool FindI5(int x)
        //{
        //    return x == 5;
        //}

        //static bool FindF72(float x)
        //{
        //    return x == 7.2f;
        //}

        //static bool FindF53(float x)
        //{
        //    return x == 5.3f;
        //}

        static void Main(string[] args)
        {
            //int i = FindUtils.FindElement<int>(
            //    new int[] { 3, 7, 5 }, x => x >= 5);
            //Console.WriteLine(i);

            //float a = FindUtils.FindElement<float>(
            //    new float[] { 3.5f, 7.2f, 5.3f }, x => x == 7.2f);
            //Console.WriteLine(a);

            //float b = FindUtils.FindElement<float>(
            //    new float[] { 3.5f, 7.2f, 5.3f }, x => x == 5.3f);
            //Console.WriteLine(b);

            //int[] array = new int[] { 3, 7, 5 };
            //Console.WriteLine("What do you want me to find?");
            //int input = int.Parse(Console.ReadLine());
            //int result = FindUtils.FindElement<int>(array, x => x == input);
            //Console.WriteLine("Result: {0}", result);

            Student[] array = new Student[]
            {
                new Student("Fred", "Flinstone", 3.2),
                new Student("Barney", "Fife", 1.0),
                new Student("Barney", "Ruble", 3.5),
                new Student("Jeff", "Black", 3.4),
            };
            Console.WriteLine("What is the students first name?");
            string input = Console.ReadLine();
            //Student result = FindUtils.FindElement(array, x => x.FirstName == input);
            //Student result = array.FindElement(x => x.FirstName == input);
            Student result = array.FirstOrDefault(x => x.FirstName == input);
            if (result == null)
            {
                Console.WriteLine("NOT FOUND");
            }
            else
            {
                Console.WriteLine("Result: {0}", result);
            }

            //var result = array.Where(x => x.FirstName == input);
            //foreach (Student s in result)
            //{
            //    Console.WriteLine("Result: {0}", s);
            //}
        }
    }
}
