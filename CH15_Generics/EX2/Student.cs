﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX2
{
    public class Student
    {
        public Student(string first, string last, double gpa)
        {
            FirstName = first;
            LastName = last;
            GPA = gpa;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public double GPA { get; set; }

        public override string ToString()
        {
            return $"{FirstName} {LastName}: {GPA}";
        }
    }
}
