﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX5
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum = 0;
            foreach (int num in GetUserInput())
            {
                sum += num;
                Console.WriteLine($"Sum: {sum}");
            }
            Console.WriteLine($"Sum: {sum}");
        }

        static IEnumerable<int> GetUserInput()
        {
            for (;;)
            {
                Console.Write("number: ");
                string input = Console.ReadLine();
                if (int.TryParse(input, out int x))
                {
                    if (x == 999)
                    {
                        yield break;
                    }
                    else
                    {
                        yield return x;
                    }
                }
                else
                {
                    Console.WriteLine("INVALID NUMBER");
                }
            }
        }
    }
}
