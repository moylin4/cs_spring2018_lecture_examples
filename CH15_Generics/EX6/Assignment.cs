﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX6
{
    [Serializable]
    public class Assignment
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public float GradeNumber { get; set; }

        public string GradeLetter
        {
            get
            {
                if (GradeNumber >= 94.5f)
                {
                    return "A";
                }
                else if (GradeNumber >= 89.5f)
                {
                    return "B+";
                }
                else if (GradeNumber >= 83.5f)
                {
                    return "B";
                }
                else if (GradeNumber >= 80.5f)
                {
                    return "C+";
                }
                else if (GradeNumber >= 74.5f)
                {
                    return "C";
                }
                else if (GradeNumber >= 69.5f)
                {
                    return "D";
                }
                else
                {
                    return "F";
                }
            }
        }

        public override string ToString()
        {
            return $"{Type} {Name}: {GradeLetter} ({GradeNumber:0.0}%)";
        }
    }
}
