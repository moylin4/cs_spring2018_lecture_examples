﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX6
{
    public partial class AssignmentsForm : Form
    {
        private List<Assignment> _assignments;

        public AssignmentsForm()
        {
            InitializeComponent();
            lblError.Text = "";

            _assignments = new List<Assignment>();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            lblError.ForeColor = SystemColors.ControlText;
            try
            {
                var a = new Assignment();
                a.Type = cmbType.SelectedItem as string;
                a.Name = txtName.Text;
                a.GradeNumber = float.Parse(txtGrade.Text);

                _assignments.Add(a);
                lstAssignments.Items.Add(a);
                lblError.Text = "Added";
            }
            catch (FormatException)
            {
                lblError.Text = "Please enter valid values. (Format)";
                lblError.ForeColor = Color.Red;
            }
            catch (OverflowException)
            {
                lblError.Text = "Please enter valid values. (Overflow)";
                lblError.ForeColor = Color.Red;
            }
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            Assignment assignment = 
                lstAssignments.SelectedItem as Assignment;
            if (assignment != null)
            {
                _assignments.Remove(assignment);
                lstAssignments.Items.Remove(assignment);
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            lblError.ForeColor = SystemColors.ControlText;
            _assignments.Clear();
            lstAssignments.Items.Clear();
            try
            {
                string path = "test.csv";
                using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    using (var reader = new StreamReader(stream))
                    {
                        string line;
                        while ((line = reader.ReadLine()) != null)
                        {
                            var a = new Assignment();
                            string[] fields = line.Split(',');
                            a.Type = fields[0];
                            a.Name = fields[1];
                            a.GradeNumber = float.Parse(fields[2]);

                            _assignments.Add(a);
                            lstAssignments.Items.Add(a);
                        }
                    }
                }
                lblError.Text = "Loaded";
            }
            catch (Exception ex)
            {
                lblError.Text = $"{ex.GetType().Name}: {ex.Message}";
                lblError.ForeColor = Color.Red;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            lblError.ForeColor = SystemColors.ControlText;
            try
            {
                string path = "test.csv";
                using (var stream = new FileStream(path, FileMode.Create, FileAccess.Write))
                {
                    using (var writer = new StreamWriter(stream))
                    {
                        foreach (Assignment a in _assignments)
                        {
                            writer.WriteLine($"{a.Type},{a.Name},{a.GradeNumber:0.0},{a.GradeLetter}");
                        }
                        writer.Flush();
                    }
                }
                lblError.Text = "Saved";
            }
            catch (Exception ex)
            {
                lblError.Text = $"{ex.GetType().Name}: {ex.Message}";
                lblError.ForeColor = Color.Red;
            }
        }
    }
}
