﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX3
{
    class Program
    {
        static void Main(string[] args)
        {
            //ResizableArray<int> ints = new ResizableArray<int>(5);
            //ints.Add(1);
            //ints.Add(5);
            //ints.Add(6);
            //ints.Add(7);
            //ints.Add(8);
            //ints.Add(3);

            //for (int i = 0; i < ints.Count; ++i)
            //{
            //    Console.WriteLine(ints.GetItem(i));
            //}

            ResizableArray<Student> students = new ResizableArray<Student>(5);
            students.Add(new Student("Jeff", "Black", 3.0));
            students.Add(new Student("Barney", "Rubble", 4.0));
            students.Add(new Student("Dexter", "Putin", 40.0));

            students[0] = new Student("Jetson", "2", 3.2);

            //for (int i = 0; i < students.Count; ++i)
            //{
            //    //if (students.GetItem(i).FirstName == "Dexter")
            //    Console.WriteLine(students[i]);
            //}

            foreach (Student s in students)
            {
                Console.WriteLine(s);
            }

        }
    }
}
