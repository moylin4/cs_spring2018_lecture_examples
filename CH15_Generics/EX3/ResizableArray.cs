﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX3
{
    public class ResizableArray<T> : IEnumerable<T>
    {
        private T[] _items;
        private int _count;

        public ResizableArray(int capacity)
        {
            _items = new T[capacity];
            _count = 0;
        }

        public ResizableArray(T[] items)
        {
            _items = new T[items.Length];
            _count = 0;
            foreach (T s in items)
            {
                _items[_count] = s;
                ++_count;
            }
        }

        public int Count
        {
            get { return _count; }
        }

        public T this[int index]
        {
            get
            {
                if (index < 0) { throw new IndexOutOfRangeException(); }
                if (index >= _count) { throw new IndexOutOfRangeException(); }
                return _items[index];
            }
            set
            {
                if (index < 0) { throw new IndexOutOfRangeException(); }
                if (index >= _count) { throw new IndexOutOfRangeException(); }
                if (value == null) { throw new NullReferenceException(); }
                _items[index] = value;
            }
        }

        //public T GetItem(int index)
        //{
        //    return _items[index];
        //}

        public void Add(T s)
        {
            if (s == null)
            {
                throw new NullReferenceException();
            }
            if (_count >= _items.Length)
            {
                Array.Resize(ref _items, _count * 2);
            }

            _items[_count] = s;
            ++_count;
        }

        public void Remove(T s)
        {
            int foundIndex = -1;
            for (int i = 0; i < _count; ++i)
            {
                if (_items[i].Equals(s))
                {
                    foundIndex = i;
                    break;
                }
            }

            if (foundIndex >= 0)
            {
                for (int i = foundIndex + 1; foundIndex < _count; ++i)
                {
                    _items[i - 1] = _items[i];
                }
                _items[_count] = default(T);
                --_count;
            }
        }

        public T Find(Func<T, bool> clause)
        {
            foreach (T item in this)
            {
                if (clause(item))
                {
                    return item;
                }
            }
            return default(T);
        }

        public IEnumerator<T> GetEnumerator()
        {
            for (int i = 0; i < _count; ++i)
            {
                yield return _items[i];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
