﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX3
{
    public class ResizableStudentArray
    {
        private Student[] _items;
        private int _count;

        public ResizableStudentArray(int capacity)
        {
            _items = new Student[capacity];
            _count = 0;
        }

        public ResizableStudentArray(Student[] items)
        {
            _items = new Student[items.Length];
            _count = 0;
            foreach (Student s in items)
            {
                _items[_count] = s;
                ++_count;
            }
        }

        public int Count
        {
            get { return _count; }
        }

        public void Add(Student s)
        {
            if (_count >= _items.Length)
            {
                Array.Resize(ref _items, _count * 2);
            }

            _items[_count] = s;
            ++_count;
        }

        public void Remove(Student s)
        {
            int foundIndex = -1;
            for (int i = 0; i < _count; ++i)
            {
                if (_items[i].Equals(s))
                {
                    foundIndex = i;
                    break;
                }
            }

            if (foundIndex >= 0)
            {
                for (int i = foundIndex + 1; foundIndex < _count; ++i)
                {
                    _items[i - 1] = _items[i];
                }
                _items[_count] = null;
                --_count;
            }
        }
    }
}
