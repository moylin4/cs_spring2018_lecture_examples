﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX3
{
    public class Student
    {
        public Student(string first, string last, double gpa)
        {
            FirstName = first;
            LastName = last;
            GPA = gpa;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public double GPA { get; set; }

        public override bool Equals(object obj)
        {
            Student other = obj as Student;
            if (other == null) { return false; }

            return this.FirstName == other.FirstName &&
                   this.LastName == other.LastName &&
                   this.GPA == other.GPA;
        }

        public override int GetHashCode()
        {
            return FirstName.GetHashCode() ^
                   LastName.GetHashCode() ^
                   GPA.GetHashCode();
        }

        public override string ToString()
        {
            return $"{FirstName} {LastName}: {GPA}";
        }
    }
}
