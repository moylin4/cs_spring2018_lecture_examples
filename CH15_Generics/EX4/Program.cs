﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX4
{
    class Program
    {
        static void Main(string[] args)
        {
            foreach (int x in MultiplesOf3(5))
            {
                Console.WriteLine(x);
            }
        }

        static IEnumerable<int> MultiplesOf3(int n)
        {
            for (int i = 1; i <= n; ++i)
            {
                yield return i * 3;
            }
        }
    }
}
