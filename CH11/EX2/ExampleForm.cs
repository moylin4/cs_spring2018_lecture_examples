﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX2
{
    public partial class ExampleForm : Form
    {
        public ExampleForm()
        {
            InitializeComponent();
        }

        private void btnDoStuff_Click(object sender, EventArgs e)
        {
            lblError1.Text = "";
            lblError2.Text = "";
            lblOutput.Text = "";

            int x;
            int y;
            if (!int.TryParse(txtInput1.Text, out x))
            {
                x = 1;
                lblError1.Text = "ERROR";
            }
            if (!int.TryParse(txtInput2.Text, out y))
            {
                y = 1;
                lblError2.Text = "ERROR";
            }

            if (y != 0)
            {
                lblOutput.Text = (x / y).ToString();
            }
            else
            {
                lblOutput.Text = "ERROR: divide by zero";
            }
        }
    }
}
