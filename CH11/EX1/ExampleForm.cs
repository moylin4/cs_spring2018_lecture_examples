﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX1
{
    public partial class ExampleForm : Form
    {
        public ExampleForm()
        {
            InitializeComponent();
        }

        private void btnDoStuff_Click(object sender, EventArgs e)
        {
            try
            {
                int x = int.Parse(txtInput1.Text);
                int y = int.Parse(txtInput2.Text);
                lblOutput.Text = (x / y).ToString();
            }
            catch (FormatException ex)
            {
                lblOutput.Text = "ERROR: please enter a number";
            }
            catch (DivideByZeroException ex)
            {
                lblOutput.Text = "ERROR: divide by zero";
            }
            catch (Exception ex)
            {
                lblOutput.Text = "ERROR: " + ex.Message;
            }
            //finally
            //{
            //    MessageBox.Show("Hello");
            //}

            //lblOutput.Text = (
            //    int.Parse(txtInput1.Text) / 
            //    int.Parse(txtInput2.Text)
            //).ToString();
        }
    }
}
