﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX1
{
    public class Box
    {
        private double _width;
        private double _height;

        public Box(double width, double height)
        {
            if (width < 1 || width > 10)
            {
                throw new ArgumentException("invalid width");
            }
            if (height < 1 || height > 10)
            {
                throw new ArgumentException("invalid height");
            }

            _width = width;
            _height = height;
        }

        public double Width
        {
            get { return _width; }
            set
            {
                if (value < 1 || value > 10)
                {
                    throw new ArgumentException("invalid width");
                }
                else
                {
                    _width = value;
                }
            }
        }

        public double Height
        {
            get { return _height; }
            set
            {
                if (value < 1 || value > 10)
                {
                    throw new ArgumentException("invalid height");
                }
                else
                {
                    _height = value;
                }
            }
        }

        public double Area
        {
            get { return _width * _height; }
        }
    }
}
