﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX1
{
    public class InvalidHeightException : ArgumentException
    {
        private double _height;

        public InvalidHeightException(
            string message, double height)
            : base(message)
        {
            _height = height;
        }

        public double Height
        {
            get { return _height; }
        }
    }
}
