﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            lblError.Text = "";
        }

        private const int COUNT = 5;
        private string[] firstNames = new string[COUNT]
        {
            "Markel",
            "Luiza",
            "Bryony",
            "Giraldo",
            "Lowri",
        };
        private string[] lastNames = new string[COUNT]
        {
            "Diggory",
            "Gunnar",
            "Hester",
            "Addy",
            "Hari",
        };
        private string[] phoneNumbers = new string[COUNT]
        {
            "555-8390",
            "555-4618",
            "555-4440",
            "555-1687",
            "555-7763",
        };

        private void btnSearch_Click(object sender, EventArgs e)
        {
            int index = SearchByName(txtNameInput.Text);
            ShowPerson(index);

            //ShowPerson(SearchByName(txtNameInput.Text));
        }

        public int SearchByName(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                return -1;
            }
            
            name = name.ToLower().Trim();
            for (int i = 0; i < COUNT; ++i)
            {
                string fullName =
                    firstNames[i].ToLower() + " " +
                    lastNames[i].ToLower();

                if (fullName.Contains(name))
                {
                    return i;
                }
            }
            return -1;
        }

        public void ShowPerson(int index)
        {
            if (index >= 0)
            {
                lblFirstName.Text = firstNames[index];
                lblLastName.Text = lastNames[index];
                lblPhoneNumber.Text = phoneNumbers[index];
                lblError.Text = "";
            }
            else
            {
                lblFirstName.Text = "error";
                lblLastName.Text = "error";
                lblPhoneNumber.Text = "error";
                lblError.Text = "User Not Found";
            }
        }
    }
}
