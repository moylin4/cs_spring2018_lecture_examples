﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX4
{
    class Program
    {
        static void Main(string[] args)
        {
            const double INTEREST_RATE = 1.04;
            double balance = 1000;
            
            for (;;)
            {
                Console.WriteLine("Do you want to see your balance for next year? Y or N...");
                string input = Console.ReadLine();
                input = input.ToLower();

                if (input == "y")
                {
                    balance *= INTEREST_RATE;
                    Console.WriteLine("Bank Balance: {0:C}", balance);
                }
                else if (input == "n")
                {
                    break;
                }
                else
                {
                    Console.WriteLine("INVALID RESPONSE");
                }
            }

            Console.WriteLine("Have a nice day!");
        }
    }
}
