﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX5
{
    class Program
    {
        static void Main(string[] args)
        {
            string input = "";
            for (int i = 0; i < 5; ++i)
            {
                Console.Write(i + ": ");
                Console.WriteLine("before: " + input);
                input = Console.ReadLine();
                Console.WriteLine("after: " + input);
            }
            Console.WriteLine("done");
        }
    }
}
