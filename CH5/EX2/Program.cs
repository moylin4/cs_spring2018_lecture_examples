﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EX2
{
    class Program
    {
        static void Main(string[] args)
        {
            const double INTEREST_RATE = 1.04;
            double balance = 1000;

            Console.WriteLine("Do you want to see your balance for next year? Y or N...");
            string input = Console.ReadLine();

            while (input != "N" && input != "n")
            {
                balance *= INTEREST_RATE;
                Console.WriteLine("Bank Balance: {0:C}", balance);
                
                Console.WriteLine("Do you want to see your balance for next year? Y or N...");
                input = Console.ReadLine();
            }

            Console.WriteLine("Have a nice day!");
        }
    }
}
