﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX6
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum = 0;
            int min = int.MaxValue;
            int max = int.MinValue;

            for (int i = 1; i <= 5; ++i)
            {
                Console.Write(i + ": ");
                string input = Console.ReadLine();
                int number = int.Parse(input);
                sum += number;

                if (number < min) { min = number; }
                if (number > max) { max = number; }
            }
            Console.WriteLine(sum);
            Console.WriteLine(sum / 5.0f);
            Console.WriteLine(min);
            Console.WriteLine(max);
            //Console.WriteLine(sum / (float)(i - 1));
            Console.ReadLine();
        }
    }
}
