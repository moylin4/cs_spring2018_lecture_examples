﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX8
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        const double INTEREST_RATE = 1.04;
        double balance = 1000;

        private void btnNextYear_Click(object sender, EventArgs e)
        {
            //double balance = 1000;
            //balance = balance * INTEREST_RATE;
            balance *= INTEREST_RATE;
            lblBalance.Text = string.Format(
                "Bank Balance: {0:C}", balance
            );
        }
    }
}
