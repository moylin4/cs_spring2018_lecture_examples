﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ShowDefaultMessage();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ShowMyMessage("Awesome!");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ShowMyMessageAndTitle("Game Over! Your Ship Esploded!", "ERORR");
        }

        private void ShowDefaultMessage()
        {
            MessageBox.Show("Default");
        }

        private void ShowMyMessage(string msg)
        {
            MessageBox.Show(msg);
        }

        private void ShowMyMessageAndTitle(
            string msg, string title)
        {
            MessageBox.Show(msg, title);
        }
    }
}
