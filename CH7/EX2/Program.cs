﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numbers = new int[100];
            int count = 0;

            count = PromptUserForData(numbers, count);

            Console.WriteLine("Numbers: {0}", count);
            for (int i = 0; i < count; ++i)
            {
                Console.WriteLine(numbers[i]);
            }

            int sum = 0;
            for (int i = 0; i < count; ++i)
            {
                sum += numbers[i];
            }

            float avg = (float)sum / count;
            Console.WriteLine("Sum: {0}", sum);
            Console.WriteLine("Avg: {0:F2}", avg);
        }

        public static int PromptUserForData(int[] data, int count)
        {
            while (count < data.Length)
            {
                Console.Write("{0:00}: ", count);
                int num = int.Parse(Console.ReadLine());
                if (num == 999)
                {
                    break;
                }
                else if (num < 200 || num > 300)
                {
                    Console.WriteLine("ERROR");
                }
                else
                {
                    data[count] = num;
                    ++count;
                }
            }
            return count;
        }
    }
}
