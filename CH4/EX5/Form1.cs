﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        int x = 0;

        private void btnIncrement_Click(object sender, EventArgs e)
        {
            //int x = 0;
            ++x;
            lblOutput.Text = string.Format("{0}", x);
        }
    }
}
