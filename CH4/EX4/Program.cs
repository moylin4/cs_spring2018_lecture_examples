﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX4
{
    class Program
    {
        static void Main(string[] args)
        {
            double saleAmount = 2000;
            double commissionRate;

            if (saleAmount >= 1000) { commissionRate = 0.08; }
            else if (saleAmount >= 500) { commissionRate = 0.06; }
            else if (saleAmount >= 200) { commissionRate = 0.05; }
            else { commissionRate = 0.03; }

            //if (saleAmount < 200) { commissionRate = 0.03; }
            //else if (saleAmount < 500) { commissionRate = 0.05; }
            //else if (saleAmount < 1000) { commissionRate = 0.06; }
            //else { commissionRate = 0.08; }

            Console.WriteLine(commissionRate);
        }
    }
}
