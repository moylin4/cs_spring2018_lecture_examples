﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EX2_TempConverter;

namespace EX2_TempConverter.Test
{
    [TestClass]
    public class TemperatureUtilsTest
    {
        [TestMethod]
        public void convert_32F_to_0C()
        {
            // Execute the scenario
            float temp = TemperatureUtils.ConvertFahrenheitToCelsius(32.0f);
            // Check the outcome
            Assert.AreEqual(0.0f, temp);
        }

        [TestMethod]
        public void convert_212F_to_100C()
        {
            // Execute the scenario
            float temp = TemperatureUtils.ConvertFahrenheitToCelsius(212.0f);
            // Check the outcome
            Assert.AreEqual(100.0f, temp);
        }

        [TestMethod]
        public void convert_0C_to_32F()
        {
            // Execute the scenario
            float temp = TemperatureUtils.ConvertCelsiusToFahrenheit(0.0f);
            // Check the outcome
            Assert.AreEqual(32.0f, temp);
        }

        [TestMethod]
        public void convert_100C_to_212F()
        {
            // Execute the scenario
            float temp = TemperatureUtils.ConvertCelsiusToFahrenheit(100.0f);
            // Check the outcome
            Assert.AreEqual(212.0f, temp);
        }
    }
}
