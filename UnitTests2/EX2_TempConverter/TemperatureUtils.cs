﻿namespace EX2_TempConverter
{
    public static class TemperatureUtils
    {
        public static float ConvertFahrenheitToCelsius(float fahrenheit)
        {
            return (fahrenheit - 32.0f) / 1.8f;
        }

        public static float ConvertCelsiusToFahrenheit(float celsius)
        {
            return (celsius * 1.8f) + 32.0f;
        }
    }
}
