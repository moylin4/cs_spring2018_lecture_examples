﻿using System;
using System.Windows.Forms;

namespace EX2_TempConverter
{
    public partial class TempConverterForm : Form
    {
        public TempConverterForm()
        {
            InitializeComponent();
        }

        private void btnConvertToCelsius_Click(object sender, EventArgs e)
        {
            float fahrenheit = float.Parse(txtFahrenheit.Text);
            float celsius = TemperatureUtils.ConvertFahrenheitToCelsius(fahrenheit);
            txtCelsius.Text = celsius.ToString("0.0");
        }

        private void btnConvertToFahrenheit_Click(object sender, EventArgs e)
        {
            float celsius = float.Parse(txtCelsius.Text);
            float fahrenheit = TemperatureUtils.ConvertCelsiusToFahrenheit(celsius);
            txtFahrenheit.Text = fahrenheit.ToString("0.0");
        }
    }
}
