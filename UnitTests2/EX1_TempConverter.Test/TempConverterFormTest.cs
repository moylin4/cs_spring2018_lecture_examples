﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EX1_TempConverter;

namespace EX1_TempConverter.Test
{
    [TestClass]
    public class TempConverterFormTest
    {
        [TestMethod]
        public void convert_32F_to_0C()
        {
            using (var form = new TempConverterForm())
            {
                // Execute the scenario
                form.Show();
                form.txtFahrenheit.Text = "32.0";
                form.btnConvertToCelsius.PerformClick();

                // Check the outcome
                Assert.AreEqual("0.0", form.txtCelsius.Text);
            }
        }

        [TestMethod]
        public void convert_212F_to_100C()
        {
            using (var form = new TempConverterForm())
            {
                // Execute the scenario
                form.Show();
                form.txtFahrenheit.Text = "212.0";
                form.btnConvertToCelsius.PerformClick();

                // Check the outcome
                Assert.AreEqual("100.0", form.txtCelsius.Text);
            }
        }

        [TestMethod]
        public void convert_0C_to_32F()
        {
            using (var form = new TempConverterForm())
            {
                // Execute the scenario
                form.Show();
                form.txtCelsius.Text = "0.0";
                form.btnConvertToFahrenheit.PerformClick();

                // Check the outcome
                Assert.AreEqual("32.0", form.txtFahrenheit.Text);
            }
        }

        [TestMethod]
        public void convert_100C_to_212F()
        {
            using (var form = new TempConverterForm())
            {
                // Execute the scenario
                form.Show();
                form.txtCelsius.Text = "100.0";
                form.btnConvertToFahrenheit.PerformClick();

                // Check the outcome
                Assert.AreEqual("212.0", form.txtFahrenheit.Text);
            }
        }
    }
}
