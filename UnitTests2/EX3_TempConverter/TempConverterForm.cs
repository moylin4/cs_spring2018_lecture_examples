﻿using System;
using System.Windows.Forms;

namespace EX3_TempConverter
{
    public partial class TempConverterForm : Form
    {
        public TempConverterForm()
        {
            InitializeComponent();
        }

        private void btnConvertToCelsius_Click(object sender, EventArgs e)
        {
            TemperatureConverter converter = new TemperatureConverter();
            converter.Fahrenheit = float.Parse(txtFahrenheit.Text);
            txtCelsius.Text = converter.Celsius.ToString("0.0");
        }

        private void btnConvertToFahrenheit_Click(object sender, EventArgs e)
        {
            TemperatureConverter converter = new TemperatureConverter();
            converter.Celsius = float.Parse(txtCelsius.Text);
            txtFahrenheit.Text = converter.Fahrenheit.ToString("0.0");
        }
    }
}
