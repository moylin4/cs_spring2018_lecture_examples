﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX3_TempConverter
{
    public class TemperatureConverter
    {
        private float _fahrenheit;
        private float _celsius;
        
        public TemperatureConverter()
        {
            _fahrenheit = 32.0f;
            _celsius = 0.0f;
        }

        public float Fahrenheit
        {
            get { return _fahrenheit; }
            set
            {
                _fahrenheit = value;
                _celsius = (value - 32.0f) / 1.8f;
            }
        }

        public float Celsius
        {
            get { return _celsius; }
            set
            {
                _celsius = value;
                _fahrenheit = (value * 1.8f) + 32.0f;
            }
        }
    }
}
