﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EX3_TempConverter;

namespace EX3_TempConverter.Test
{
    [TestClass]
    public class TemperatureConverterTest
    {
        [TestMethod]
        public void convert_32F_to_0C()
        {
            // Execute the scenario
            TemperatureConverter converter = new TemperatureConverter();
            converter.Fahrenheit = 32.0f;

            // Check the outcome
            Assert.AreEqual(32.0f, converter.Fahrenheit);
            Assert.AreEqual(0.0f, converter.Celsius);
        }

        [TestMethod]
        public void convert_212F_to_100C()
        {
            // Execute the scenario
            TemperatureConverter converter = new TemperatureConverter();
            converter.Fahrenheit = 212.0f;

            // Check the outcome
            Assert.AreEqual(212.0f, converter.Fahrenheit);
            Assert.AreEqual(100.0f, converter.Celsius);
        }

        [TestMethod]
        public void convert_0C_to_32F()
        {
            // Execute the scenario
            TemperatureConverter converter = new TemperatureConverter();
            converter.Celsius = 0.0f;

            // Check the outcome
            Assert.AreEqual(0.0f, converter.Celsius);
            Assert.AreEqual(32.0f, converter.Fahrenheit);
        }

        [TestMethod]
        public void convert_100C_to_212F()
        {
            // Execute the scenario
            TemperatureConverter converter = new TemperatureConverter();
            converter.Celsius = 100.0f;

            // Check the outcome
            Assert.AreEqual(100.0f, converter.Celsius);
            Assert.AreEqual(212.0f, converter.Fahrenheit);
        }
    }
}
