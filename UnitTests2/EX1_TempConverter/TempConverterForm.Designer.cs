﻿namespace EX1_TempConverter
{
    partial class TempConverterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblFahrenheit = new System.Windows.Forms.Label();
            this.txtFahrenheit = new System.Windows.Forms.TextBox();
            this.lblCelsius = new System.Windows.Forms.Label();
            this.txtCelsius = new System.Windows.Forms.TextBox();
            this.btnConvertToCelsius = new System.Windows.Forms.Button();
            this.btnConvertToFahrenheit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblFahrenheit
            // 
            this.lblFahrenheit.AutoSize = true;
            this.lblFahrenheit.Location = new System.Drawing.Point(22, 9);
            this.lblFahrenheit.Name = "lblFahrenheit";
            this.lblFahrenheit.Size = new System.Drawing.Size(173, 17);
            this.lblFahrenheit.TabIndex = 0;
            this.lblFahrenheit.Text = "Temperature in fahrenheit";
            // 
            // txtFahrenheit
            // 
            this.txtFahrenheit.Location = new System.Drawing.Point(25, 29);
            this.txtFahrenheit.Name = "txtFahrenheit";
            this.txtFahrenheit.Size = new System.Drawing.Size(170, 22);
            this.txtFahrenheit.TabIndex = 1;
            // 
            // lblCelsius
            // 
            this.lblCelsius.AutoSize = true;
            this.lblCelsius.Location = new System.Drawing.Point(239, 9);
            this.lblCelsius.Name = "lblCelsius";
            this.lblCelsius.Size = new System.Drawing.Size(152, 17);
            this.lblCelsius.TabIndex = 2;
            this.lblCelsius.Text = "Temperature in celsius";
            // 
            // txtCelsius
            // 
            this.txtCelsius.Location = new System.Drawing.Point(242, 29);
            this.txtCelsius.Name = "txtCelsius";
            this.txtCelsius.Size = new System.Drawing.Size(170, 22);
            this.txtCelsius.TabIndex = 3;
            // 
            // btnConvertToCelsius
            // 
            this.btnConvertToCelsius.Location = new System.Drawing.Point(25, 57);
            this.btnConvertToCelsius.Name = "btnConvertToCelsius";
            this.btnConvertToCelsius.Size = new System.Drawing.Size(170, 42);
            this.btnConvertToCelsius.TabIndex = 4;
            this.btnConvertToCelsius.Text = "Convert to celsius";
            this.btnConvertToCelsius.UseVisualStyleBackColor = true;
            this.btnConvertToCelsius.Click += new System.EventHandler(this.btnConvertToCelsius_Click);
            // 
            // btnConvertToFahrenheit
            // 
            this.btnConvertToFahrenheit.Location = new System.Drawing.Point(242, 57);
            this.btnConvertToFahrenheit.Name = "btnConvertToFahrenheit";
            this.btnConvertToFahrenheit.Size = new System.Drawing.Size(170, 42);
            this.btnConvertToFahrenheit.TabIndex = 5;
            this.btnConvertToFahrenheit.Text = "Convert to fahrenheit";
            this.btnConvertToFahrenheit.UseVisualStyleBackColor = true;
            this.btnConvertToFahrenheit.Click += new System.EventHandler(this.btnConvertToFahrenheit_Click);
            // 
            // TempConverterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(446, 125);
            this.Controls.Add(this.btnConvertToFahrenheit);
            this.Controls.Add(this.btnConvertToCelsius);
            this.Controls.Add(this.txtCelsius);
            this.Controls.Add(this.lblCelsius);
            this.Controls.Add(this.txtFahrenheit);
            this.Controls.Add(this.lblFahrenheit);
            this.Name = "TempConverterForm";
            this.Text = "Temperature Converter";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox txtFahrenheit;
        public System.Windows.Forms.Label lblCelsius;
        public System.Windows.Forms.TextBox txtCelsius;
        public System.Windows.Forms.Button btnConvertToCelsius;
        public System.Windows.Forms.Button btnConvertToFahrenheit;
        public System.Windows.Forms.Label lblFahrenheit;
    }
}

