﻿using System;
using System.IO;

namespace EX1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter a filename >> ");
            string fileName = Console.ReadLine();
            if (File.Exists(fileName))
            {
                Console.WriteLine("File Exists");
                Console.WriteLine(
                    "Created {0}", File.GetCreationTime(fileName)
                );
                Console.WriteLine(
                    "Access {0}", File.GetLastAccessTime(fileName)
                );
                Console.WriteLine(
                    "Written {0}", File.GetLastWriteTime(fileName)
                );
            }
            else
            {
                Console.WriteLine("File does not exist");
            }
        }
    }
}
