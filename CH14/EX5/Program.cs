﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace EX5
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = "test.bin";
            BinaryFormatter formatter = new BinaryFormatter();

            // Take user input
            Employee emp = new Employee();
            Console.Write("Employee Number >> ");
            emp.EmployeeNumber = int.Parse(Console.ReadLine());
            Console.Write("Employee Name >> ");
            emp.Name = Console.ReadLine();
            Console.Write("Employee Salary >> ");
            emp.Salary = double.Parse(Console.ReadLine());

            // Save the file
            using (var stream = new FileStream(fileName, FileMode.Create, FileAccess.Write))
            {
                formatter.Serialize(stream, emp);
            }

            // Load the file
            using (var stream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                emp = (Employee)formatter.Deserialize(stream);
                Console.WriteLine($"{emp.EmployeeNumber} {emp.Name} {emp.Salary}");
            }
        }
    }
}
