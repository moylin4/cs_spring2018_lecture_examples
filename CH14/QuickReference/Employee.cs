﻿using System;

namespace QuickReference
{
    [Serializable]
    public class Employee
    {
        public int EmployeeNumber { get; set; }
        public string Name { get; set; }
        public double Salary { get; set; }

        public Employee()
        {
        }

        public Employee(int employeeNumber, string name, double salary)
        {
            EmployeeNumber = employeeNumber;
            Name = name;
            Salary = salary;
        }
    }
}
