﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Web.Script.Serialization;

namespace QuickReference
{
    public static class EmployeeStorage
    {
        #region Binary

        /**
         * Save a list of employee objects to a binary file.
         */
        public static bool SaveBinaryFile(string path, List<Employee> employees)
        {
            using (var stream = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                var f = new BinaryFormatter();
                f.Serialize(stream, employees);
                stream.Flush();
                return true;
            }
        }

        /**
         * Load a list of employee objects from a binary file.
         */
        public static List<Employee> LoadBinaryFile(string path)
        {
            using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                var f = new BinaryFormatter();
                List<Employee> employees = (List<Employee>)f.Deserialize(stream);
                return employees;
            }
        }

        #endregion

        #region JSON

        /**
         * Save a list of employee objects to a JSON file.
         */
        public static bool SaveJsonFile(string path, List<Employee> employees)
        {
            using (var stream = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                using (var writer = new StreamWriter(stream))
                {
                    var s = new JavaScriptSerializer();
                    string json = s.Serialize(employees);

                    writer.WriteLine(json);
                    writer.Flush();
                    return true;
                }
            }
        }

        /**
         * Load a list of employee objects from a JSON file.
         */
        public static List<Employee> LoadJsonFile(string path)
        {
            using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                using (var reader = new StreamReader(stream))
                {
                    var s = new JavaScriptSerializer();
                    string json = reader.ReadToEnd();
                    List<Employee> employees = s.Deserialize<List<Employee>>(json);
                    return employees;
                }
            }
        }

        #endregion

        #region CSV

        public const char CSV_DELIM = ',';

        /**
         * Save a list of employee objects to a CSV file.
         */
        public static bool SaveCsvFile(string path, List<Employee> employees)
        {
            StringBuilder sb = new StringBuilder();
            using (var stream = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                using (var writer = new StreamWriter(stream))
                {
                    foreach (Employee emp in employees)
                    {
                        sb.Clear();
                        sb.Append(emp.EmployeeNumber);
                        sb.Append(CSV_DELIM);
                        sb.Append(emp.Name);
                        sb.Append(CSV_DELIM);
                        sb.Append(emp.Salary.ToString("0.00"));

                        writer.WriteLine(sb.ToString());
                    }
                    writer.Flush();
                    return true;
                }
            }
        }

        /**
         * Load a list of employee objects from a CSV file.
         */
        public static List<Employee> LoadCsvFile(string path)
        {
            List<Employee> employees = new List<Employee>();

            using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                using (var reader = new StreamReader(stream))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        string[] fields = line.Split(CSV_DELIM);

                        Employee emp = new Employee();
                        emp.EmployeeNumber = int.Parse(fields[0]);
                        emp.Name = fields[1];
                        emp.Salary = double.Parse(fields[2]);

                        employees.Add(emp);
                    }
                }
            }

            return employees;
        }

        #endregion
    }
}
