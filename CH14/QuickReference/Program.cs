﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickReference
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // Create a list of employees
                var employees = new List<Employee>();
                employees.Add(new Employee(1, "Fred Jordan", 10000));
                employees.Add(new Employee(2, "Bill Johnson", 20000));
                employees.Add(new Employee(2, "Eddie Murphy", 30000));

                // Save all three file formats
                EmployeeStorage.SaveJsonFile("employees.json", employees);
                EmployeeStorage.SaveBinaryFile("employees.bin", employees);
                EmployeeStorage.SaveCsvFile("employees.csv", employees);

                // Load the JSON file
                Console.WriteLine("JSON");
                foreach (var e in EmployeeStorage.LoadJsonFile("employees.json"))
                {
                    Console.WriteLine($"{e.EmployeeNumber}, {e.Name}, {e.Salary:C}");
                }
                Console.WriteLine();

                // Load the binary file
                Console.WriteLine("Binary");
                foreach (var e in EmployeeStorage.LoadBinaryFile("employees.bin"))
                {
                    Console.WriteLine($"{e.EmployeeNumber}, {e.Name}, {e.Salary:C}");
                }
                Console.WriteLine();

                // Load the CSV file
                Console.WriteLine("CSV");
                foreach (var e in EmployeeStorage.LoadCsvFile("employees.csv"))
                {
                    Console.WriteLine($"{e.EmployeeNumber}, {e.Name}, {e.Salary:C}");
                }
                Console.WriteLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
