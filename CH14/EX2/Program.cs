﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX2
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = "test.txt";
            using (var stream = new FileStream(fileName, FileMode.Append, FileAccess.Write))
            {
                using (var writer = new StreamWriter(stream))
                {
                    for (;;)
                    {
                        Console.Write("Enter some text >> ");
                        string text = Console.ReadLine();
                        if (text == "999") { break; }
                        writer.WriteLine(text);
                    }
                    
                    writer.Flush();
                }
            }
        }
    }
}
