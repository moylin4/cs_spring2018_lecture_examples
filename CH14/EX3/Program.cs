﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX3
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = "test.txt";
            using (var stream = new FileStream(fileName, FileMode.Append, FileAccess.Write))
            {
                using (var writer = new StreamWriter(stream))
                {
                    for (;;)
                    {
                        Console.Write("Employee Number >> ");
                        string employeeNumber = Console.ReadLine();
                        if (employeeNumber == "999") { break; }

                        Console.Write("Employee Name >> ");
                        string employeeName = Console.ReadLine();
                        Console.Write("Employee Salary >> ");
                        double employeeSalary = double.Parse(Console.ReadLine());
                        
                        writer.WriteLine("{0},{1},{2}", employeeNumber, employeeName, employeeSalary);
                    }

                    writer.Flush();
                }
            }
        }
    }
}
