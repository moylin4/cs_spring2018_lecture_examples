﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace EX7
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            lblError.Text = "";
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Employee Files (*.emp)|*.emp";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                LoadFile(dlg.FileName);
            }
        }

        public void LoadFile(string path)
        {
            lblError.Text = "";
            try
            {
                using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    using (var reader = new StreamReader(stream))
                    {
                        string json = reader.ReadToEnd();
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        Employee emp = serializer.Deserialize<Employee>(json);

                        txtEmployeeNumber.Text = emp.EmployeeNumber.ToString();
                        txtEmployeeName.Text = emp.Name;
                        txtEmployeeSalary.Text = emp.Salary.ToString("0.00");
                    }
                }
            }
            catch (Exception ex)
            {
                lblError.Text = $"{ex.GetType().Name}: {ex.Message}";
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog dlg = new SaveFileDialog();
            dlg.Filter = "Employee Files (*.emp)|*.emp";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                SaveFile(dlg.FileName);
            }
        }

        public void SaveFile(string path)
        {
            lblError.Text = "";
            try
            {
                using (var stream = new FileStream(path, FileMode.Create, FileAccess.Write))
                {
                    using (var writer = new StreamWriter(stream))
                    {
                        JavaScriptSerializer serializer = new JavaScriptSerializer();
                        
                        Employee emp = new Employee();
                        emp.EmployeeNumber = int.Parse(txtEmployeeNumber.Text);
                        emp.Name = txtEmployeeName.Text;
                        emp.Salary = double.Parse(txtEmployeeSalary.Text);
                        
                        string json = serializer.Serialize(emp);
                        writer.Write(json);
                        writer.Flush();
                    }
                }
            }
            catch (Exception ex)
            {
                lblError.Text = $"{ex.GetType().Name}: {ex.Message}";
            }
        }
    }
}
