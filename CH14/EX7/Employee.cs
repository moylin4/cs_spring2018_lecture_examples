﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX7
{
    [Serializable]
    public class Employee
    {
        public int EmployeeNumber { get; set; }
        public string Name { get; set; }
        public double Salary { get; set; }
    }
}
