﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX4
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = "test.txt";
            try
            {
                using (var stream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                {
                    using (var reader = new StreamReader(stream))
                    {
                        Console.WriteLine("{0,-3} {1,-10} {2,-10}", "Num", "Name", "Salary");

                        string line = null;
                        while ((line = reader.ReadLine()) != null)
                        {
                            string[] fields = line.Split(',');
                            string employeeNumber = fields[0];
                            string employeeName = fields[1];
                            double employeeSalary = double.Parse(fields[2]);

                            //Console.WriteLine("{0,-3} {1,-10} {2,-10:C}", employeeNumber, employeeName, employeeSalary);
                            Console.WriteLine($"{employeeNumber,-3} {employeeName,-10} {employeeSalary,-10:C}");
                        }
                    }
                }
            }
            catch (IOException ex)
            {
                Console.WriteLine($"{ex.GetType().Name}: {ex.Message}");
            }
        }
    }
}
