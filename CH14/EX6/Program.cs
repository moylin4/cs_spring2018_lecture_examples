﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace EX6
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = "test.json";
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            // Take user input
            Employee emp = new Employee();
            Console.Write("Employee Number >> ");
            emp.EmployeeNumber = int.Parse(Console.ReadLine());
            Console.Write("Employee Name >> ");
            emp.Name = Console.ReadLine();
            Console.Write("Employee Salary >> ");
            emp.Salary = double.Parse(Console.ReadLine());

            // Save the file
            using (var stream = new FileStream(fileName, FileMode.Create, FileAccess.Write))
            {
                using (var writer = new StreamWriter(stream))
                {
                    string json = serializer.Serialize(emp);
                    writer.Write(json);
                    writer.Flush();
                }
            }

            // Load the file
            using (var stream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
            {
                using (var reader = new StreamReader(stream))
                {
                    string json = reader.ReadToEnd();
                    emp = serializer.Deserialize<Employee>(json);
                }
            }
            Console.WriteLine($"{emp.EmployeeNumber} {emp.Name} {emp.Salary}");
        }
    }
}
