﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace EX1
{
    public partial class InventoryScreen : UserControl
    {
        private MainForm _form;

        public InventoryScreen()
        {
            InitializeComponent();
        }

        public MainForm Form
        {
            get { return _form; }
            set { _form = value; }
        }

        private void btnCombat_Click(object sender, EventArgs e)
        {
            _form.NextLevel();
            _form.SwitchScreen(ScreenId.COMBAT);

            var snd = new SoundPlayer(
                EX1.Properties.Resources.Pickup_Coin);
            snd.Play();
        }

        private void InventoryScreen_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible && _form != null)
            {
                lblScreenName.Text = $"Inventory ({_form.Level})";
            }
        }
    }
}
