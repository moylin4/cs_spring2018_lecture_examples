﻿namespace EX1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.combatScreen = new EX1.CombatScreen();
            this.inventoryScreen = new EX1.InventoryScreen();
            this.SuspendLayout();
            // 
            // combatScreen
            // 
            this.combatScreen.BackColor = System.Drawing.Color.Red;
            this.combatScreen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.combatScreen.Form = null;
            this.combatScreen.Location = new System.Drawing.Point(0, 0);
            this.combatScreen.Name = "combatScreen";
            this.combatScreen.Size = new System.Drawing.Size(525, 464);
            this.combatScreen.TabIndex = 0;
            this.combatScreen.Visible = false;
            // 
            // inventoryScreen
            // 
            this.inventoryScreen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.inventoryScreen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.inventoryScreen.Form = null;
            this.inventoryScreen.Location = new System.Drawing.Point(0, 0);
            this.inventoryScreen.Name = "inventoryScreen";
            this.inventoryScreen.Size = new System.Drawing.Size(525, 464);
            this.inventoryScreen.TabIndex = 1;
            this.inventoryScreen.Visible = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 464);
            this.Controls.Add(this.inventoryScreen);
            this.Controls.Add(this.combatScreen);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private CombatScreen combatScreen;
        private InventoryScreen inventoryScreen;
    }
}

