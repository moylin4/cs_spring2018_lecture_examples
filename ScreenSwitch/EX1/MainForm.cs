﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX1
{
    public partial class MainForm : Form
    {
        private int _level = 0;

        public MainForm()
        {
            InitializeComponent();

            combatScreen.Form = this;
            inventoryScreen.Form = this;

            combatScreen.Visible = true;
        }

        public int Level
        {
            get { return _level; }
        }

        public void NextLevel()
        {
            ++_level;
        }

        public void SwitchScreen(ScreenId screen)
        {
            // Hide all screens
            combatScreen.Visible = false;
            inventoryScreen.Visible = false;

            // Show current screen
            switch (screen)
            {
                case ScreenId.COMBAT:
                    combatScreen.Visible = true;
                    break;
                case ScreenId.INVENTORY:
                    inventoryScreen.Visible = true;
                    break;
            }
        }
    }
}
