﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX1
{
    public partial class CombatScreen : UserControl
    {
        private MainForm _form;

        public CombatScreen()
        {
            InitializeComponent();
        }

        public MainForm Form
        {
            get { return _form; }
            set { _form = value; }
        }

        private void btnInventory_Click(object sender, EventArgs e)
        {
            _form.SwitchScreen(ScreenId.INVENTORY);
        }

        private void CombatScreen_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible && _form != null)
            {
                lblScreenName.Text = $"Combat ({_form.Level})";
            }
        }
    }
}
