﻿namespace EX1
{
    partial class InventoryScreen
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCombat = new System.Windows.Forms.Button();
            this.lblScreenName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnCombat
            // 
            this.btnCombat.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCombat.Location = new System.Drawing.Point(65, 240);
            this.btnCombat.Name = "btnCombat";
            this.btnCombat.Size = new System.Drawing.Size(271, 76);
            this.btnCombat.TabIndex = 3;
            this.btnCombat.Text = "Combat";
            this.btnCombat.UseVisualStyleBackColor = true;
            this.btnCombat.Click += new System.EventHandler(this.btnCombat_Click);
            // 
            // lblScreenName
            // 
            this.lblScreenName.AutoSize = true;
            this.lblScreenName.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblScreenName.Location = new System.Drawing.Point(49, 84);
            this.lblScreenName.Name = "lblScreenName";
            this.lblScreenName.Size = new System.Drawing.Size(331, 82);
            this.lblScreenName.TabIndex = 2;
            this.lblScreenName.Text = "Inventory";
            // 
            // InventoryScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.Controls.Add(this.btnCombat);
            this.Controls.Add(this.lblScreenName);
            this.Name = "InventoryScreen";
            this.Size = new System.Drawing.Size(608, 554);
            this.VisibleChanged += new System.EventHandler(this.InventoryScreen_VisibleChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCombat;
        private System.Windows.Forms.Label lblScreenName;
    }
}
