﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX1
{
    public class Student
    {
        public string Name { get; set; }
        public int Semester { get; set; }
        public float Gpa { get; set; }
    }
}
