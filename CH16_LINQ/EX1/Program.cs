﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX1
{
    class Program
    {
        static void Main(string[] args)
        {
            var students = new List<Student>();
            students.Add(new Student { Name = "Coyote", Semester = 1, Gpa = 2.0f });
            students.Add(new Student { Name = "Fred Flinstone", Semester = 2, Gpa = 3.0f });
            students.Add(new Student { Name = "Barney Fife", Semester = 3, Gpa = 1.5f });
            students.Add(new Student { Name = "Mr. Jetson", Semester = 4, Gpa = 3.5f });

            //Console.WriteLine("Semester");
            //int semester = int.Parse(Console.ReadLine());

            //var query =
            //    from s in students
            //    //where s.Semester == semester
            //    where s.Name != "Coyote"
            //    orderby s.Name
            //    select s;

            //foreach (Student stu in query)
            //{
            //    Console.WriteLine($"{stu.Name}, {stu.Semester}, {stu.Gpa:0.0}");
            //}

            //var query =
            //    from s in students
            //    where s.Name != "Coyote"
            //    orderby s.Name descending
            //    select new { s.Name, s.Gpa };

            //var query =
            //    students.Where(x => x.Name != "Coyote")
            //            .OrderBy(x => x.Gpa);

            var query =
                students.OrderByDescending(x => x.Gpa).Take(2);

            Console.WriteLine($"any: {query.Any()}");

            foreach (var stu in query)
            {
                Console.WriteLine($"{stu.Name}, {stu.Gpa:0.0}");
            }

            Console.WriteLine();
            var first = query.Skip(1).FirstOrDefault();
            Console.WriteLine($"{first.Name}, {first.Gpa:0.0}");

            Console.WriteLine();
            float count = query.Count();
            Console.WriteLine($"Count: {count}");
            float sumGpa = query.Sum(x => x.Gpa);
            Console.WriteLine($"Sum: {sumGpa}");
            float averageGpa = query.Average(x => x.Gpa);
            Console.WriteLine($"Avg: {averageGpa}");
        }
    }
}
