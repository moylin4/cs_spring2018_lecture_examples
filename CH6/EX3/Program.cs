﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX3
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] minQuantitiesForDiscount = new int[5] {
                1, 13, 50, 100, 200
            };
            float[] discountRanges = new float[5] {
                0.0f, 0.1f, 0.14f, 0.18f, 0.20f
            };

            Console.WriteLine("How many gidgets do you want?");
            int gidgets = int.Parse(Console.ReadLine());
            
            float discount = 0.0f;
            //for (int i = 0; i < minQuantitiesForDiscount.Length; ++i)
            for (int i = minQuantitiesForDiscount.Length - 1; i >= 0; --i)
            {
                if (gidgets >= minQuantitiesForDiscount[i])
                {
                    discount = discountRanges[i];
                    break;
                }
            }

            Console.WriteLine("{0} gidgets at a discount of {1:P0}", gidgets, discount);
        }
    }
}
