﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            lblOutput.Text = "";
        }

        int[] itemIds = new int[10]
        {
            101, 108, 201, 213, 266, 304, 311, 409, 411, 412
        };
        float[] itemPrices = new float[10]
        {
            0.89f, 1.23f, 3.50f, 0.69f, 5.79f, 3.19f, 0.99f, 0.89f, 1.26f, 8.00f
        };

        private void btnSearch_Click(object sender, EventArgs e)
        {
            int id = int.Parse(txtItemNumber.Text);
            //bool found = false;
            int foundIndex = -1;
            //for (int i = 0; i < itemIds.Length; ++i)
            for (int i = itemIds.Length - 1; i >= 0; --i)
            {
                if (id == itemIds[i])
                {
                    //found = true;
                    foundIndex = i;
                    break;
                }
            }

            if (foundIndex >= 0)
            {
                lblOutput.Text = string.Format(
                    "VALID\n{0}\n{1:C}",
                    itemIds[foundIndex],
                    itemPrices[foundIndex]
                );
            }
            else
            {
                lblOutput.Text = "INVALID";
            }

            //lblOutput.Text = 
            //    (found ? "VALID" : "INVALID") +
            //    string.Format("\n{0:C}", itemPrices[i]);
        }
    }
}
