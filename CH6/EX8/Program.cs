﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX8
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            int[] arr = new int[1000000];
            for (int i = 0; i < arr.Length; ++i)
            {
                arr[i] = rand.Next(0, 100000);
            }

            Array.Sort(arr);
            var watch = Stopwatch.StartNew();
            int search = 42;
            int foundIndex = Array.BinarySearch(arr, search);
            Console.WriteLine(foundIndex);
            Console.WriteLine(watch.Elapsed);

            //var watch = Stopwatch.StartNew();
            //int search = 42;
            //int foundIndex = -1;
            //for (int i = 0; i < arr.Length; ++i)
            //{
            //    if (search == arr[i])
            //    {
            //        foundIndex = i;
            //        break;
            //    }
            //}
            //Console.WriteLine(foundIndex);
            //Console.WriteLine(watch.Elapsed);
        }
    }
}
