﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX5
{
    class Program
    {
        static void Main(string[] args)
        {
            StringBuilder output = new StringBuilder();

            for (int i = 0; i < 1000000; ++i)
            {
                output.AppendFormat("Line {0}\n", i);
            }

            Console.WriteLine(output.ToString());
        }
    }
}
