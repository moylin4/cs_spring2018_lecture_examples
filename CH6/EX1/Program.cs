﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX1
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] firstNames = new string[3] { "A", "B", "C" };
            string[] lastNames = new string[3] { "D", "E", "F" };
            int[] ages = new int[3] { 1, 2, 3 };

            for (int i = 0; i < firstNames.Length; ++i)
            {
                Console.WriteLine("{0} {1} {2}", firstNames[i], lastNames[i], ages[i]);
            }
        }
    }
}
