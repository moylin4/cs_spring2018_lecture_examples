﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX4
{
    class Program
    {
        static void Main(string[] args)
        {
            string output = "";

            for (int i = 0; i < 1000000; ++i)
            {
                output += string.Format("Line {0}\n", i);
            }

            Console.WriteLine(output);
        }
    }
}
