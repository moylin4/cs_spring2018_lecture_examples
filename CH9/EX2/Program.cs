﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX2
{
    class Program
    {
        static void Main(string[] args)
        {
            Paycheck p = new Paycheck(
                10.0f, 40.0f
            );
            Console.WriteLine(
                "{0:C}/hr x {1:F1} hrs = {2:C}",
                p.GetHourlyWage(), p.GetHoursWorked(), p.GetAmount()
            );

            p.SetHourlyWage(15.0f);
            p.SetHoursWorked(35.0f);
            Console.WriteLine(
                "{0:C}/hr x {1:F1} hrs = {2:C}",
                p.GetHourlyWage(), p.GetHoursWorked(), p.GetAmount()
            );
        }
    }
}
