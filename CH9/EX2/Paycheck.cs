﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX2
{
    public class Paycheck
    {
        private float _hourlyWage;
        private float _hoursWorked;
        private float _paycheckAmount;

        public Paycheck(float rate, float hours)
        {
            _hourlyWage = rate;
            _hoursWorked = hours;
            _paycheckAmount = rate * hours;
        }

        public float GetHourlyWage() { return _hourlyWage; }
        public float GetHoursWorked() { return _hoursWorked; }
        public float GetAmount() { return _paycheckAmount; }

        public void SetHourlyWage(float value)
        {
            _hourlyWage = value;
            _paycheckAmount = _hourlyWage * _hoursWorked;
        }
        public void SetHoursWorked(float value)
        {
            _hoursWorked = value;
            _paycheckAmount = _hourlyWage * _hoursWorked;
        }
    }
}
