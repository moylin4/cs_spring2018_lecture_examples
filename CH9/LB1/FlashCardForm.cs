﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB1
{
    public partial class FlashCardForm : Form
    {
        private int _cardCount;
        private FlashCard[] _cards;
        
        public FlashCardForm()
        {
            InitializeComponent();
            _cardCount = 0;
            _cards = new FlashCard[20];
            AddDefinition(
                "variable", "A variable is a named memory location."
            );
            AddDefinition(
                "exception", "An exception is when something unexpected occurs."
            );
            // TODO: Add 3 more definitions here

            lblDefinition.Text = "";
            txtDefinition.Visible = false;
            btnAdd.Visible = false;
        }

        private void btnDefine_Click(object sender, EventArgs e)
        {
            string term = txtTerm.Text;
            if (string.IsNullOrWhiteSpace(term))
            {
                lblDefinition.Text = "Please enter a term.";
                txtDefinition.Text = "";
                txtDefinition.Visible = false;
                btnAdd.Visible = false;
                return;
            }

            string definition = FindDefinition(term);
            if (!string.IsNullOrWhiteSpace(definition))
            {
                lblDefinition.Text = definition;
                txtDefinition.Text = "";
                txtDefinition.Visible = false;
                btnAdd.Visible = false;
            }
            else
            {
                lblDefinition.Text = "I don't know that term yet. What does it mean?";
                txtDefinition.Text = "";
                txtDefinition.Visible = true;
                btnAdd.Visible = true;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (AddDefinition(txtTerm.Text, txtDefinition.Text))
            {
                lblDefinition.Text = "Definition Added";
                txtDefinition.Text = "";
                txtDefinition.Visible = false;
                btnAdd.Visible = false;
            }
            else
            {
                lblDefinition.Text = "List is full";
            }
        }

        public string FindDefinition(string term)
        {
            term = term.ToLower();
            for (int i = 0; i < _cardCount; ++i)
            {
                if (_cards[i].GetTerm().ToLower().Contains(term))
                {
                    return _cards[i].GetDefinition();
                }
            }
            return "";
        }

        public bool AddDefinition(string term, string definition)
        {
            if (_cardCount < _cards.Length)
            {
                FlashCard card = new FlashCard(term, definition);
                _cards[_cardCount] = card;
                ++_cardCount;
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
