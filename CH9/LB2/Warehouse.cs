﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB2
{
    public class Warehouse
    {
        private int _count;
        private Product[] _products;
        private string[] _accessKeys =
        {
            "1234", "5678", "1111"
        };

        public Warehouse()
        {
            _count = 0;
            _products = new Product[20];

            // TEST DATA
            _products[0] = new Product(
                "Bottle Rocket", "1234-5678", 15, 100, 50, "Black Cat"
            );
            _products[1] = new Product(
                "TNT", "1000-1000", 100, 90, 1, "Acme"
            );
            _count = 2;
        }

        public Product SearchByName(string name)
        {
            for (int i = 0; i < _count; ++i)
            {
                if (_products[i].Name == name)
                {
                    return _products[i];
                }
            }
            return null;
        }

        public Product SearchByUPC(string upc)
        {
            for (int i = 0; i < _count; ++i)
            {
                if (_products[i].UPC == upc)
                {
                    return _products[i];
                }
            }
            return null;
        }

        public bool AddProduct(Product p)
        {
            return false;
        }

        public bool DeleteProduct(Product p)
        {
            bool found = false;
            int i = 0;
            for (; i < _count; ++i)
            {
                if (_products[i] == p)
                {
                    found = true;
                    break;
                }
            }
            if (found)
            {
                ++i;
                for (; i < _count; ++i)
                {
                    _products[i - 1] = _products[i];
                }
                --_count;
                _products[_count] = null;
            }
            return found;
        }

        public bool CheckAccessKey(string key)
        {
            for (int i = 0; i < _accessKeys.Length; ++i)
            {
                if (_accessKeys[i] == key)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
