﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB2
{
    public partial class WarehouseForm : Form
    {
        private Warehouse _warehouse;
        private Product _selectedProduct;

        public WarehouseForm()
        {
            InitializeComponent();
            _warehouse = new Warehouse();
            lblChangePriceError.Text = "";
            lblDeleteItemError.Text = "";
        }

        private void ShowProduct(Product p)
        {
            _selectedProduct = p;
            if (p != null)
            {
                lblQueryName.Text = string.Format("Name: {0}", p.Name);
                lblQueryUPC.Text = string.Format("UPC: {0}", p.UPC);
                lblQueryStorePrice.Text = string.Format("Store Price: {0:C}", p.StorePrice);
                lblQueryCostPerCase.Text = string.Format("Cost Per Case: {0:C}", p.CostPerCase);
                lblQueryUnitsPerCase.Text = string.Format("Units Per Case: {0}", p.UnitsPerCase);
                lblQueryDistributor.Text = string.Format("Distributor: {0}", p.Distributor);
            }
            else
            {
                lblQueryName.Text = "Name: Not Found";
                lblQueryUPC.Text = "UPC: Not Found";
                lblQueryStorePrice.Text = "Store Price: Not Found";
                lblQueryCostPerCase.Text = "Cost Per Case: Not Found";
                lblQueryUnitsPerCase.Text = "Units Per Case: Not Found";
                lblQueryDistributor.Text = "Distributor: Not Found";
            }
        }

        private void btnSearchByName_Click(object sender, EventArgs e)
        {
            Product p = _warehouse.SearchByName(txtSearchByName.Text);
            ShowProduct(p);
        }

        private void btnSearchByUPC_Click(object sender, EventArgs e)
        {
            Product p = _warehouse.SearchByUPC(txtSearchByUPC.Text);
            ShowProduct(p);
        }

        private void btnUpdatePrice_Click(object sender, EventArgs e)
        {
            float price = float.Parse(txtNewPrice.Text);
            string key = txtChangePriceKey.Text;
            if (_selectedProduct != null && 
                _warehouse.CheckAccessKey(key))
            {
                _selectedProduct.StorePrice = price;
                ShowProduct(_selectedProduct);
                lblChangePriceError.Text = "";
            }
            else
            {
                lblChangePriceError.Text = "Access Key Incorrect";
            }
        }

        private void btnDeleteItem_Click(object sender, EventArgs e)
        {
            string upc = txtDeleteItemUPC.Text;
            string key = txtDeleteItemKey.Text;
            if (_selectedProduct != null &&
                _selectedProduct.UPC == upc &&
                _warehouse.CheckAccessKey(key))
            {
                _warehouse.DeleteProduct(_selectedProduct);
                ShowProduct(null);
                lblDeleteItemError.Text = "";
            }
            else
            {
                lblDeleteItemError.Text = "Access Key Incorrect";
            }
        }
    }
}
