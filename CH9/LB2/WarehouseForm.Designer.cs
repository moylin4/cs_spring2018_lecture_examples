﻿namespace LB2
{
    partial class WarehouseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpSelectedItem = new System.Windows.Forms.GroupBox();
            this.grpChangePrice = new System.Windows.Forms.GroupBox();
            this.grpDeleteItem = new System.Windows.Forms.GroupBox();
            this.btnSearchByName = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSearchByName = new System.Windows.Forms.TextBox();
            this.btnSearchByUPC = new System.Windows.Forms.Button();
            this.txtSearchByUPC = new System.Windows.Forms.TextBox();
            this.lblQueryName = new System.Windows.Forms.Label();
            this.lblQueryUPC = new System.Windows.Forms.Label();
            this.lblQueryStorePrice = new System.Windows.Forms.Label();
            this.lblQueryCostPerCase = new System.Windows.Forms.Label();
            this.lblQueryUnitsPerCase = new System.Windows.Forms.Label();
            this.lblQueryDistributor = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtNewPrice = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtChangePriceKey = new System.Windows.Forms.TextBox();
            this.btnUpdatePrice = new System.Windows.Forms.Button();
            this.lblChangePriceError = new System.Windows.Forms.Label();
            this.lblDeleteItemError = new System.Windows.Forms.Label();
            this.btnDeleteItem = new System.Windows.Forms.Button();
            this.txtDeleteItemKey = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDeleteItemUPC = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.grpSelectedItem.SuspendLayout();
            this.grpChangePrice.SuspendLayout();
            this.grpDeleteItem.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpSelectedItem
            // 
            this.grpSelectedItem.Controls.Add(this.lblQueryDistributor);
            this.grpSelectedItem.Controls.Add(this.lblQueryUnitsPerCase);
            this.grpSelectedItem.Controls.Add(this.lblQueryCostPerCase);
            this.grpSelectedItem.Controls.Add(this.lblQueryStorePrice);
            this.grpSelectedItem.Controls.Add(this.lblQueryUPC);
            this.grpSelectedItem.Controls.Add(this.lblQueryName);
            this.grpSelectedItem.Location = new System.Drawing.Point(23, 81);
            this.grpSelectedItem.Name = "grpSelectedItem";
            this.grpSelectedItem.Size = new System.Drawing.Size(792, 132);
            this.grpSelectedItem.TabIndex = 0;
            this.grpSelectedItem.TabStop = false;
            this.grpSelectedItem.Text = "Selected Item";
            // 
            // grpChangePrice
            // 
            this.grpChangePrice.Controls.Add(this.lblChangePriceError);
            this.grpChangePrice.Controls.Add(this.btnUpdatePrice);
            this.grpChangePrice.Controls.Add(this.txtChangePriceKey);
            this.grpChangePrice.Controls.Add(this.label10);
            this.grpChangePrice.Controls.Add(this.txtNewPrice);
            this.grpChangePrice.Controls.Add(this.label9);
            this.grpChangePrice.Location = new System.Drawing.Point(24, 237);
            this.grpChangePrice.Name = "grpChangePrice";
            this.grpChangePrice.Size = new System.Drawing.Size(385, 207);
            this.grpChangePrice.TabIndex = 1;
            this.grpChangePrice.TabStop = false;
            this.grpChangePrice.Text = "Change Store Price";
            // 
            // grpDeleteItem
            // 
            this.grpDeleteItem.Controls.Add(this.lblDeleteItemError);
            this.grpDeleteItem.Controls.Add(this.btnDeleteItem);
            this.grpDeleteItem.Controls.Add(this.txtDeleteItemKey);
            this.grpDeleteItem.Controls.Add(this.label13);
            this.grpDeleteItem.Controls.Add(this.txtDeleteItemUPC);
            this.grpDeleteItem.Controls.Add(this.label14);
            this.grpDeleteItem.Location = new System.Drawing.Point(431, 237);
            this.grpDeleteItem.Name = "grpDeleteItem";
            this.grpDeleteItem.Size = new System.Drawing.Size(384, 207);
            this.grpDeleteItem.TabIndex = 2;
            this.grpDeleteItem.TabStop = false;
            this.grpDeleteItem.Text = "Delete Item";
            // 
            // btnSearchByName
            // 
            this.btnSearchByName.Location = new System.Drawing.Point(316, 32);
            this.btnSearchByName.Name = "btnSearchByName";
            this.btnSearchByName.Size = new System.Drawing.Size(89, 35);
            this.btnSearchByName.TabIndex = 3;
            this.btnSearchByName.Text = "Search";
            this.btnSearchByName.UseVisualStyleBackColor = true;
            this.btnSearchByName.Click += new System.EventHandler(this.btnSearchByName_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 20);
            this.label1.TabIndex = 4;
            this.label1.Text = "Search by Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(427, 12);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 20);
            this.label2.TabIndex = 5;
            this.label2.Text = "Search by UPC";
            // 
            // txtSearchByName
            // 
            this.txtSearchByName.Location = new System.Drawing.Point(24, 36);
            this.txtSearchByName.Name = "txtSearchByName";
            this.txtSearchByName.Size = new System.Drawing.Size(286, 26);
            this.txtSearchByName.TabIndex = 6;
            // 
            // btnSearchByUPC
            // 
            this.btnSearchByUPC.Location = new System.Drawing.Point(722, 32);
            this.btnSearchByUPC.Name = "btnSearchByUPC";
            this.btnSearchByUPC.Size = new System.Drawing.Size(89, 35);
            this.btnSearchByUPC.TabIndex = 4;
            this.btnSearchByUPC.Text = "Search";
            this.btnSearchByUPC.UseVisualStyleBackColor = true;
            this.btnSearchByUPC.Click += new System.EventHandler(this.btnSearchByUPC_Click);
            // 
            // txtSearchByUPC
            // 
            this.txtSearchByUPC.Location = new System.Drawing.Point(431, 36);
            this.txtSearchByUPC.Name = "txtSearchByUPC";
            this.txtSearchByUPC.Size = new System.Drawing.Size(286, 26);
            this.txtSearchByUPC.TabIndex = 7;
            // 
            // lblQueryName
            // 
            this.lblQueryName.AutoSize = true;
            this.lblQueryName.Location = new System.Drawing.Point(7, 37);
            this.lblQueryName.Name = "lblQueryName";
            this.lblQueryName.Size = new System.Drawing.Size(55, 20);
            this.lblQueryName.TabIndex = 0;
            this.lblQueryName.Text = "Name:";
            // 
            // lblQueryUPC
            // 
            this.lblQueryUPC.AutoSize = true;
            this.lblQueryUPC.Location = new System.Drawing.Point(7, 67);
            this.lblQueryUPC.Name = "lblQueryUPC";
            this.lblQueryUPC.Size = new System.Drawing.Size(46, 20);
            this.lblQueryUPC.TabIndex = 1;
            this.lblQueryUPC.Text = "UPC:";
            // 
            // lblQueryStorePrice
            // 
            this.lblQueryStorePrice.AutoSize = true;
            this.lblQueryStorePrice.Location = new System.Drawing.Point(7, 97);
            this.lblQueryStorePrice.Name = "lblQueryStorePrice";
            this.lblQueryStorePrice.Size = new System.Drawing.Size(91, 20);
            this.lblQueryStorePrice.TabIndex = 2;
            this.lblQueryStorePrice.Text = "Store Price:";
            // 
            // lblQueryCostPerCase
            // 
            this.lblQueryCostPerCase.AutoSize = true;
            this.lblQueryCostPerCase.Location = new System.Drawing.Point(277, 37);
            this.lblQueryCostPerCase.Name = "lblQueryCostPerCase";
            this.lblQueryCostPerCase.Size = new System.Drawing.Size(115, 20);
            this.lblQueryCostPerCase.TabIndex = 3;
            this.lblQueryCostPerCase.Text = "Cost Per Case:";
            // 
            // lblQueryUnitsPerCase
            // 
            this.lblQueryUnitsPerCase.AutoSize = true;
            this.lblQueryUnitsPerCase.Location = new System.Drawing.Point(277, 67);
            this.lblQueryUnitsPerCase.Name = "lblQueryUnitsPerCase";
            this.lblQueryUnitsPerCase.Size = new System.Drawing.Size(119, 20);
            this.lblQueryUnitsPerCase.TabIndex = 4;
            this.lblQueryUnitsPerCase.Text = "Units Per Case:";
            // 
            // lblQueryDistributor
            // 
            this.lblQueryDistributor.AutoSize = true;
            this.lblQueryDistributor.Location = new System.Drawing.Point(277, 97);
            this.lblQueryDistributor.Name = "lblQueryDistributor";
            this.lblQueryDistributor.Size = new System.Drawing.Size(86, 20);
            this.lblQueryDistributor.TabIndex = 5;
            this.lblQueryDistributor.Text = "Distributor:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 48);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 20);
            this.label9.TabIndex = 0;
            this.label9.Text = "New Price";
            // 
            // txtNewPrice
            // 
            this.txtNewPrice.Location = new System.Drawing.Point(120, 48);
            this.txtNewPrice.Name = "txtNewPrice";
            this.txtNewPrice.Size = new System.Drawing.Size(139, 26);
            this.txtNewPrice.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 94);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 20);
            this.label10.TabIndex = 2;
            this.label10.Text = "Access Key";
            // 
            // txtChangePriceKey
            // 
            this.txtChangePriceKey.Location = new System.Drawing.Point(120, 94);
            this.txtChangePriceKey.Name = "txtChangePriceKey";
            this.txtChangePriceKey.Size = new System.Drawing.Size(139, 26);
            this.txtChangePriceKey.TabIndex = 3;
            // 
            // btnUpdatePrice
            // 
            this.btnUpdatePrice.Location = new System.Drawing.Point(51, 126);
            this.btnUpdatePrice.Name = "btnUpdatePrice";
            this.btnUpdatePrice.Size = new System.Drawing.Size(132, 35);
            this.btnUpdatePrice.TabIndex = 4;
            this.btnUpdatePrice.Text = "Update Price";
            this.btnUpdatePrice.UseVisualStyleBackColor = true;
            this.btnUpdatePrice.Click += new System.EventHandler(this.btnUpdatePrice_Click);
            // 
            // lblChangePriceError
            // 
            this.lblChangePriceError.AutoSize = true;
            this.lblChangePriceError.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChangePriceError.ForeColor = System.Drawing.Color.Red;
            this.lblChangePriceError.Location = new System.Drawing.Point(25, 164);
            this.lblChangePriceError.Name = "lblChangePriceError";
            this.lblChangePriceError.Size = new System.Drawing.Size(178, 20);
            this.lblChangePriceError.TabIndex = 5;
            this.lblChangePriceError.Text = "Access Key Incorrect";
            // 
            // lblDeleteItemError
            // 
            this.lblDeleteItemError.AutoSize = true;
            this.lblDeleteItemError.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeleteItemError.ForeColor = System.Drawing.Color.Red;
            this.lblDeleteItemError.Location = new System.Drawing.Point(32, 158);
            this.lblDeleteItemError.Name = "lblDeleteItemError";
            this.lblDeleteItemError.Size = new System.Drawing.Size(178, 20);
            this.lblDeleteItemError.TabIndex = 11;
            this.lblDeleteItemError.Text = "Access Key Incorrect";
            // 
            // btnDeleteItem
            // 
            this.btnDeleteItem.Location = new System.Drawing.Point(58, 120);
            this.btnDeleteItem.Name = "btnDeleteItem";
            this.btnDeleteItem.Size = new System.Drawing.Size(132, 35);
            this.btnDeleteItem.TabIndex = 10;
            this.btnDeleteItem.Text = "Delete Item";
            this.btnDeleteItem.UseVisualStyleBackColor = true;
            this.btnDeleteItem.Click += new System.EventHandler(this.btnDeleteItem_Click);
            // 
            // txtDeleteItemKey
            // 
            this.txtDeleteItemKey.Location = new System.Drawing.Point(130, 85);
            this.txtDeleteItemKey.Name = "txtDeleteItemKey";
            this.txtDeleteItemKey.Size = new System.Drawing.Size(139, 26);
            this.txtDeleteItemKey.TabIndex = 9;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(17, 88);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(91, 20);
            this.label13.TabIndex = 8;
            this.label13.Text = "Access Key";
            // 
            // txtDeleteItemUPC
            // 
            this.txtDeleteItemUPC.Location = new System.Drawing.Point(130, 39);
            this.txtDeleteItemUPC.Name = "txtDeleteItemUPC";
            this.txtDeleteItemUPC.Size = new System.Drawing.Size(139, 26);
            this.txtDeleteItemUPC.TabIndex = 7;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(17, 42);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 20);
            this.label14.TabIndex = 6;
            this.label14.Text = "Verify UPC";
            // 
            // WarehouseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(823, 476);
            this.Controls.Add(this.txtSearchByUPC);
            this.Controls.Add(this.btnSearchByUPC);
            this.Controls.Add(this.txtSearchByName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSearchByName);
            this.Controls.Add(this.grpDeleteItem);
            this.Controls.Add(this.grpChangePrice);
            this.Controls.Add(this.grpSelectedItem);
            this.Name = "WarehouseForm";
            this.Text = "Store Inventory & Price Inquiry";
            this.grpSelectedItem.ResumeLayout(false);
            this.grpSelectedItem.PerformLayout();
            this.grpChangePrice.ResumeLayout(false);
            this.grpChangePrice.PerformLayout();
            this.grpDeleteItem.ResumeLayout(false);
            this.grpDeleteItem.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpSelectedItem;
        private System.Windows.Forms.Label lblQueryCostPerCase;
        private System.Windows.Forms.Label lblQueryStorePrice;
        private System.Windows.Forms.Label lblQueryUPC;
        private System.Windows.Forms.Label lblQueryName;
        private System.Windows.Forms.GroupBox grpChangePrice;
        private System.Windows.Forms.GroupBox grpDeleteItem;
        private System.Windows.Forms.Button btnSearchByName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSearchByName;
        private System.Windows.Forms.Button btnSearchByUPC;
        private System.Windows.Forms.TextBox txtSearchByUPC;
        private System.Windows.Forms.Label lblQueryDistributor;
        private System.Windows.Forms.Label lblQueryUnitsPerCase;
        private System.Windows.Forms.Label lblChangePriceError;
        private System.Windows.Forms.Button btnUpdatePrice;
        private System.Windows.Forms.TextBox txtChangePriceKey;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtNewPrice;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblDeleteItemError;
        private System.Windows.Forms.Button btnDeleteItem;
        private System.Windows.Forms.TextBox txtDeleteItemKey;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtDeleteItemUPC;
        private System.Windows.Forms.Label label14;
    }
}

