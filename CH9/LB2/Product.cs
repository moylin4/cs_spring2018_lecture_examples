﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB2
{
    public class Product
    {
        private string _name;
        private string _upc;
        private float _storePrice;
        private float _costPerCase;
        private int _unitsPerCase;
        private string _distributor;

        public Product(
            string name,
            string upc,
            float storePrice,
            float costPerCase,
            int unitsPerCase,
            string distributor
        )
        {
            _name = name;
            _upc = upc;
            _storePrice = storePrice;
            _costPerCase = costPerCase;
            _unitsPerCase = unitsPerCase;
            _distributor = distributor;
        }

        public string Name
        {
            get { return _name; }
        }
        public string UPC
        {
            get { return _upc; }
        }
        public float CostPerCase
        {
            get { return _costPerCase; }
        }
        public int UnitsPerCase
        {
            get { return _unitsPerCase; }
        }
        public string Distributor
        {
            get { return _distributor; }
        }
        public float StorePrice
        {
            get { return _storePrice; }
            set { _storePrice = value; }
        }
    }
}
