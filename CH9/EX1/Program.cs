﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX1
{
    class Program
    {
        static void Main(string[] args)
        {
            Person p = new Person(
                "Markel", "Diggory", "555-8390"
            );
            //p.firstName = "Markel";
            //p.lastName = "Diggory";
            //p.phoneNumber = "555-8390";
            Console.WriteLine(
                "{0}, {1} ({2})",
                p.lastName, p.firstName, p.phoneNumber
            );
        }
    }
}
