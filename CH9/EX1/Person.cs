﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX1
{
    public class Person
    {
        public string firstName;
        public string lastName;
        public string phoneNumber;

        public Person(string f, string l, string p)
        {
            firstName = f;
            lastName = l;
            phoneNumber = p;
        }
    }
}
