﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB11
{
    public class Order
    {
        private String _name;
        private Sundae _sundae;
        private Soda _soda;

        public Order(
            String name,
            bool hasSundae,
            bool hasSoda)
        {
            _name = name;
            if (hasSundae)
            {
                _sundae = new Sundae();
            }
            if (hasSoda)
            {
                _soda = new Soda();
            }
        }

        public string Name
        {
            get { return _name; }
        }

        public Sundae Sundae
        {
            get { return _sundae; }
        }

        public Soda Soda
        {
            get { return _soda; }
        }

        public double Price
        {
            //get { return _sundae.Price + _soda.Price; }
            get
            {
                double price = 0;
                if (_sundae != null) { price += _sundae.Price; }
                if (_soda != null) { price += _soda.Price; }
                return price;
            }
        }
    }
}
