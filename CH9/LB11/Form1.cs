﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LB11
{
    public partial class Form1 : Form
    {
        private decimal _totalCost = 0;

        public Form1()
        {
            InitializeComponent();
            ResetOrder();
        }

        private void btnAddToOrder_Click(object sender, EventArgs e)
        {
            string name = txtNameInput.Text;
            bool hasSundae = chkSundae.Checked;
            bool hasSoda = chkSoda.Checked;

            Order order = new Order(name, hasSundae, hasSoda);

            if (hasSundae)
            {
                if (chkSprinkles.Checked)
                {
                    order.Sundae.AddTopping(SundaeTopping.SPRINKLES);
                }
                if (chkChoclateSyrup.Checked)
                {
                    order.Sundae.AddTopping(SundaeTopping.CHOCLATE_SYRUP);
                }
                if (chkNuts.Checked)
                {
                    order.Sundae.AddTopping(SundaeTopping.NUTS);
                }
            }

            if (hasSoda)
            {
                if (chkLime.Checked)
                {
                    order.Soda.AddFlavor(SodaFlavor.LIME);
                }
                if (chkPeach.Checked)
                {
                    order.Soda.AddFlavor(SodaFlavor.PEACH);
                }
                if (chkMango.Checked)
                {
                    order.Soda.AddFlavor(SodaFlavor.MANGO);
                }
            }

            // Print receipt
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(order.Name);
            sb.AppendLine("----------------------");
            if (hasSundae)
            {
                sb.AppendFormat("Sundae - {0:C}\n", order.Sundae.Price);
            }
            if (hasSoda)
            {
                sb.AppendFormat("Soda - {0:C}\n", order.Soda.Price);
            }
            sb.AppendLine();
            lblReceipt.Text += sb.ToString();

            // Update total cost
            _totalCost += (decimal)order.Price;
            lblTotalCost.Text = string.Format("Total: {0:C}", _totalCost);

            // Clear out previous order
            txtNameInput.Text = "";
            chkSundae.Checked = false;
            chkSoda.Checked = false;
            chkSprinkles.Checked = false;
            // ...
            txtNameInput.Focus();
        }

        private void btnResetOrder_Click(object sender, EventArgs e)
        {
            ResetOrder();
        }

        public void ResetOrder()
        {
            _totalCost = 0;

            lblNameError.Text = "";
            lblSundaeToppingError.Text = "";
            lblDrinkMixinError.Text = "";
            lblReceipt.Text = "";
            lblTotalCost.Text = string.Format("Total: {0:C}", _totalCost);

            txtNameInput.Focus();
        }
    }
}
