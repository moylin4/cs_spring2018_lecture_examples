﻿namespace LB11
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblNamePrompt = new System.Windows.Forms.Label();
            this.txtNameInput = new System.Windows.Forms.TextBox();
            this.lblNameError = new System.Windows.Forms.Label();
            this.lblSundaePrompt = new System.Windows.Forms.Label();
            this.chkSundae = new System.Windows.Forms.CheckBox();
            this.lblSundaeToppingPrompt = new System.Windows.Forms.Label();
            this.chkSprinkles = new System.Windows.Forms.CheckBox();
            this.chkNuts = new System.Windows.Forms.CheckBox();
            this.chkChoclateSyrup = new System.Windows.Forms.CheckBox();
            this.lblSodaPrompt = new System.Windows.Forms.Label();
            this.lblDrinkMixinPrompt = new System.Windows.Forms.Label();
            this.chkSoda = new System.Windows.Forms.CheckBox();
            this.chkLime = new System.Windows.Forms.CheckBox();
            this.chkPeach = new System.Windows.Forms.CheckBox();
            this.chkMango = new System.Windows.Forms.CheckBox();
            this.lblSundaeToppingError = new System.Windows.Forms.Label();
            this.lblDrinkMixinError = new System.Windows.Forms.Label();
            this.btnAddToOrder = new System.Windows.Forms.Button();
            this.lblReceipt = new System.Windows.Forms.Label();
            this.lblOrder = new System.Windows.Forms.Label();
            this.lblTotalCost = new System.Windows.Forms.Label();
            this.btnResetOrder = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblNamePrompt
            // 
            this.lblNamePrompt.AutoSize = true;
            this.lblNamePrompt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNamePrompt.Location = new System.Drawing.Point(9, 15);
            this.lblNamePrompt.Name = "lblNamePrompt";
            this.lblNamePrompt.Size = new System.Drawing.Size(314, 20);
            this.lblNamePrompt.TabIndex = 0;
            this.lblNamePrompt.Text = "What name do you want on the order?";
            // 
            // txtNameInput
            // 
            this.txtNameInput.Location = new System.Drawing.Point(13, 35);
            this.txtNameInput.Name = "txtNameInput";
            this.txtNameInput.Size = new System.Drawing.Size(247, 20);
            this.txtNameInput.TabIndex = 1;
            // 
            // lblNameError
            // 
            this.lblNameError.AutoSize = true;
            this.lblNameError.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameError.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.lblNameError.Location = new System.Drawing.Point(9, 58);
            this.lblNameError.Name = "lblNameError";
            this.lblNameError.Size = new System.Drawing.Size(120, 20);
            this.lblNameError.TabIndex = 2;
            this.lblNameError.Text = "Name Required";
            // 
            // lblSundaePrompt
            // 
            this.lblSundaePrompt.AutoSize = true;
            this.lblSundaePrompt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSundaePrompt.Location = new System.Drawing.Point(13, 91);
            this.lblSundaePrompt.Name = "lblSundaePrompt";
            this.lblSundaePrompt.Size = new System.Drawing.Size(141, 13);
            this.lblSundaePrompt.TabIndex = 3;
            this.lblSundaePrompt.Text = "Do you want a sundae?";
            // 
            // chkSundae
            // 
            this.chkSundae.AutoSize = true;
            this.chkSundae.Location = new System.Drawing.Point(16, 116);
            this.chkSundae.Name = "chkSundae";
            this.chkSundae.Size = new System.Drawing.Size(44, 17);
            this.chkSundae.TabIndex = 4;
            this.chkSundae.Text = "Yes";
            this.chkSundae.UseVisualStyleBackColor = true;
            // 
            // lblSundaeToppingPrompt
            // 
            this.lblSundaeToppingPrompt.AutoSize = true;
            this.lblSundaeToppingPrompt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSundaeToppingPrompt.Location = new System.Drawing.Point(12, 149);
            this.lblSundaeToppingPrompt.Name = "lblSundaeToppingPrompt";
            this.lblSundaeToppingPrompt.Size = new System.Drawing.Size(106, 13);
            this.lblSundaeToppingPrompt.TabIndex = 5;
            this.lblSundaeToppingPrompt.Text = "Sundae Toppings";
            // 
            // chkSprinkles
            // 
            this.chkSprinkles.AutoSize = true;
            this.chkSprinkles.Location = new System.Drawing.Point(16, 165);
            this.chkSprinkles.Name = "chkSprinkles";
            this.chkSprinkles.Size = new System.Drawing.Size(69, 17);
            this.chkSprinkles.TabIndex = 6;
            this.chkSprinkles.Text = "Sprinkles";
            this.chkSprinkles.UseVisualStyleBackColor = true;
            // 
            // chkNuts
            // 
            this.chkNuts.AutoSize = true;
            this.chkNuts.Location = new System.Drawing.Point(16, 188);
            this.chkNuts.Name = "chkNuts";
            this.chkNuts.Size = new System.Drawing.Size(48, 17);
            this.chkNuts.TabIndex = 7;
            this.chkNuts.Text = "Nuts";
            this.chkNuts.UseVisualStyleBackColor = true;
            // 
            // chkChoclateSyrup
            // 
            this.chkChoclateSyrup.AutoSize = true;
            this.chkChoclateSyrup.Location = new System.Drawing.Point(16, 211);
            this.chkChoclateSyrup.Name = "chkChoclateSyrup";
            this.chkChoclateSyrup.Size = new System.Drawing.Size(98, 17);
            this.chkChoclateSyrup.TabIndex = 8;
            this.chkChoclateSyrup.Text = "Choclate Syrup";
            this.chkChoclateSyrup.UseVisualStyleBackColor = true;
            // 
            // lblSodaPrompt
            // 
            this.lblSodaPrompt.AutoSize = true;
            this.lblSodaPrompt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSodaPrompt.Location = new System.Drawing.Point(225, 91);
            this.lblSodaPrompt.Name = "lblSodaPrompt";
            this.lblSodaPrompt.Size = new System.Drawing.Size(127, 13);
            this.lblSodaPrompt.TabIndex = 10;
            this.lblSodaPrompt.Text = "Do you want a soda?";
            // 
            // lblDrinkMixinPrompt
            // 
            this.lblDrinkMixinPrompt.AutoSize = true;
            this.lblDrinkMixinPrompt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDrinkMixinPrompt.Location = new System.Drawing.Point(225, 149);
            this.lblDrinkMixinPrompt.Name = "lblDrinkMixinPrompt";
            this.lblDrinkMixinPrompt.Size = new System.Drawing.Size(76, 13);
            this.lblDrinkMixinPrompt.TabIndex = 12;
            this.lblDrinkMixinPrompt.Text = "Drink Mixins";
            // 
            // chkSoda
            // 
            this.chkSoda.AutoSize = true;
            this.chkSoda.Location = new System.Drawing.Point(228, 116);
            this.chkSoda.Name = "chkSoda";
            this.chkSoda.Size = new System.Drawing.Size(44, 17);
            this.chkSoda.TabIndex = 11;
            this.chkSoda.Text = "Yes";
            this.chkSoda.UseVisualStyleBackColor = true;
            // 
            // chkLime
            // 
            this.chkLime.AutoSize = true;
            this.chkLime.Location = new System.Drawing.Point(228, 165);
            this.chkLime.Name = "chkLime";
            this.chkLime.Size = new System.Drawing.Size(80, 17);
            this.chkLime.TabIndex = 13;
            this.chkLime.Text = "Lime Flavor";
            this.chkLime.UseVisualStyleBackColor = true;
            // 
            // chkPeach
            // 
            this.chkPeach.AutoSize = true;
            this.chkPeach.Location = new System.Drawing.Point(228, 188);
            this.chkPeach.Name = "chkPeach";
            this.chkPeach.Size = new System.Drawing.Size(89, 17);
            this.chkPeach.TabIndex = 14;
            this.chkPeach.Text = "Peach Flavor";
            this.chkPeach.UseVisualStyleBackColor = true;
            // 
            // chkMango
            // 
            this.chkMango.AutoSize = true;
            this.chkMango.Location = new System.Drawing.Point(228, 211);
            this.chkMango.Name = "chkMango";
            this.chkMango.Size = new System.Drawing.Size(91, 17);
            this.chkMango.TabIndex = 15;
            this.chkMango.Text = "Mango Flavor";
            this.chkMango.UseVisualStyleBackColor = true;
            // 
            // lblSundaeToppingError
            // 
            this.lblSundaeToppingError.AutoSize = true;
            this.lblSundaeToppingError.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSundaeToppingError.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.lblSundaeToppingError.Location = new System.Drawing.Point(12, 241);
            this.lblSundaeToppingError.Name = "lblSundaeToppingError";
            this.lblSundaeToppingError.Size = new System.Drawing.Size(175, 20);
            this.lblSundaeToppingError.TabIndex = 9;
            this.lblSundaeToppingError.Text = "Only 2 toppings allowed";
            // 
            // lblDrinkMixinError
            // 
            this.lblDrinkMixinError.AutoSize = true;
            this.lblDrinkMixinError.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDrinkMixinError.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.lblDrinkMixinError.Location = new System.Drawing.Point(224, 241);
            this.lblDrinkMixinError.Name = "lblDrinkMixinError";
            this.lblDrinkMixinError.Size = new System.Drawing.Size(149, 20);
            this.lblDrinkMixinError.TabIndex = 16;
            this.lblDrinkMixinError.Text = "Only 1 mixin allowed";
            // 
            // btnAddToOrder
            // 
            this.btnAddToOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddToOrder.Location = new System.Drawing.Point(15, 274);
            this.btnAddToOrder.Name = "btnAddToOrder";
            this.btnAddToOrder.Size = new System.Drawing.Size(274, 39);
            this.btnAddToOrder.TabIndex = 17;
            this.btnAddToOrder.Text = "Add Items to Order";
            this.btnAddToOrder.UseVisualStyleBackColor = true;
            this.btnAddToOrder.Click += new System.EventHandler(this.btnAddToOrder_Click);
            // 
            // lblReceipt
            // 
            this.lblReceipt.BackColor = System.Drawing.Color.White;
            this.lblReceipt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblReceipt.Location = new System.Drawing.Point(394, 35);
            this.lblReceipt.Name = "lblReceipt";
            this.lblReceipt.Size = new System.Drawing.Size(299, 226);
            this.lblReceipt.TabIndex = 19;
            this.lblReceipt.Text = "receipt";
            // 
            // lblOrder
            // 
            this.lblOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrder.Location = new System.Drawing.Point(527, 8);
            this.lblOrder.Name = "lblOrder";
            this.lblOrder.Size = new System.Drawing.Size(166, 23);
            this.lblOrder.TabIndex = 18;
            this.lblOrder.Text = "Order";
            this.lblOrder.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblTotalCost
            // 
            this.lblTotalCost.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalCost.Location = new System.Drawing.Point(394, 274);
            this.lblTotalCost.Name = "lblTotalCost";
            this.lblTotalCost.Size = new System.Drawing.Size(299, 23);
            this.lblTotalCost.TabIndex = 20;
            this.lblTotalCost.Text = "Total: $0.00";
            this.lblTotalCost.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btnResetOrder
            // 
            this.btnResetOrder.Location = new System.Drawing.Point(555, 309);
            this.btnResetOrder.Name = "btnResetOrder";
            this.btnResetOrder.Size = new System.Drawing.Size(138, 24);
            this.btnResetOrder.TabIndex = 21;
            this.btnResetOrder.Text = "Reset Order";
            this.btnResetOrder.UseVisualStyleBackColor = true;
            this.btnResetOrder.Click += new System.EventHandler(this.btnResetOrder_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(711, 350);
            this.Controls.Add(this.btnResetOrder);
            this.Controls.Add(this.lblTotalCost);
            this.Controls.Add(this.lblOrder);
            this.Controls.Add(this.lblReceipt);
            this.Controls.Add(this.btnAddToOrder);
            this.Controls.Add(this.lblDrinkMixinError);
            this.Controls.Add(this.lblSundaeToppingError);
            this.Controls.Add(this.chkMango);
            this.Controls.Add(this.chkPeach);
            this.Controls.Add(this.chkLime);
            this.Controls.Add(this.chkSoda);
            this.Controls.Add(this.lblDrinkMixinPrompt);
            this.Controls.Add(this.lblSodaPrompt);
            this.Controls.Add(this.chkChoclateSyrup);
            this.Controls.Add(this.chkNuts);
            this.Controls.Add(this.chkSprinkles);
            this.Controls.Add(this.lblSundaeToppingPrompt);
            this.Controls.Add(this.chkSundae);
            this.Controls.Add(this.lblSundaePrompt);
            this.Controls.Add(this.lblNameError);
            this.Controls.Add(this.txtNameInput);
            this.Controls.Add(this.lblNamePrompt);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblNamePrompt;
        private System.Windows.Forms.TextBox txtNameInput;
        private System.Windows.Forms.Label lblNameError;
        private System.Windows.Forms.Label lblSundaePrompt;
        private System.Windows.Forms.CheckBox chkSundae;
        private System.Windows.Forms.Label lblSundaeToppingPrompt;
        private System.Windows.Forms.CheckBox chkSprinkles;
        private System.Windows.Forms.CheckBox chkNuts;
        private System.Windows.Forms.CheckBox chkChoclateSyrup;
        private System.Windows.Forms.Label lblSodaPrompt;
        private System.Windows.Forms.Label lblDrinkMixinPrompt;
        private System.Windows.Forms.CheckBox chkSoda;
        private System.Windows.Forms.CheckBox chkLime;
        private System.Windows.Forms.CheckBox chkPeach;
        private System.Windows.Forms.CheckBox chkMango;
        private System.Windows.Forms.Label lblSundaeToppingError;
        private System.Windows.Forms.Label lblDrinkMixinError;
        private System.Windows.Forms.Button btnAddToOrder;
        private System.Windows.Forms.Label lblReceipt;
        private System.Windows.Forms.Label lblOrder;
        private System.Windows.Forms.Label lblTotalCost;
        private System.Windows.Forms.Button btnResetOrder;
    }
}

