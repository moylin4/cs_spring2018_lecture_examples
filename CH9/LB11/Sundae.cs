﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LB11
{
    public class Sundae
    {
        public const double BASE_PRICE = 4.50;
        public const double TOPPING_PRICE = 0.50;

        private SundaeTopping[] _toppings;
        private int _toppingCount;
        private double _price;

        public Sundae()
        {
            _toppings = new SundaeTopping[2];
            _toppingCount = 0;
            _price = BASE_PRICE;
        }

        public int ToppingCount
        {
            get { return _toppingCount; }
        }

        public double Price
        {
            get { return _price; }
        }

        public SundaeTopping GetTopping(int index)
        {
            return _toppings[index];
        }

        public void AddTopping(SundaeTopping t)
        {
            if (_toppingCount < _toppings.Length)
            {
                _toppings[_toppingCount] = t;
                ++_toppingCount;
                _price += TOPPING_PRICE;
            }
            else
            {
                // ERROR: throw exception (ch11)
            }
        }
    }
}
