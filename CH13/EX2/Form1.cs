﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            lblOutput.Text = "";
        }

        private void btnGreet_Click(object sender, EventArgs e)
        {
            Greeting h = Hello;
            Greeting g = Goodbye;
            ShowGreeting(h, "Cathy");
            ShowGreeting(g, "Bob");
        }

        // Greeting delegate definition
        public delegate void Greeting(string name);

        // Method that accepts a Greeting
        public void ShowGreeting(Greeting g, string name)
        {
            g(name);
            lblOutput.Text += "---\r\n";
        }

        // Greeting method
        public void Hello(string name)
        {
            lblOutput.Text += string.Format("Hello, {0}!\r\n", name);
        }

        // Greeting method
        public void Goodbye(string name)
        {
            lblOutput.Text += string.Format("Goodbye, {0}!\r\n", name);
        }
    }
}
