﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void OnTextBoxEnter(object sender, EventArgs e)
        {
            Control c = (Control)sender;
            //c.BackColor = Color.LightGreen;

            switch (c.Name)
            {
                case "textBox1":
                    c.ForeColor = Color.Black;
                    c.BackColor = Color.LightGreen;
                    break;
                case "textBox2":
                    c.ForeColor = Color.Black;
                    c.BackColor = Color.Magenta;
                    break;
                case "textBox3":
                    c.ForeColor = Color.Black;
                    c.BackColor = Color.Yellow;
                    break;
                case "textBox4":
                    c.ForeColor = Color.Black;
                    c.BackColor = Color.Orange;
                    break;
            }
        }

        private void OnTextBoxLeave(object sender, EventArgs e)
        {
            Control c = (Control)sender;
            c.BackColor = Color.Red;
        }
    }
}
