﻿namespace EX4
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNewId = new System.Windows.Forms.TextBox();
            this.txtNewGpa = new System.Windows.Forms.TextBox();
            this.lblNewValues = new System.Windows.Forms.Label();
            this.lblId = new System.Windows.Forms.Label();
            this.lblGpa = new System.Windows.Forms.Label();
            this.lblCurrentValues = new System.Windows.Forms.Label();
            this.lblCurId = new System.Windows.Forms.Label();
            this.lblCurGpa = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.lblError = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtNewId
            // 
            this.txtNewId.Location = new System.Drawing.Point(65, 28);
            this.txtNewId.Name = "txtNewId";
            this.txtNewId.Size = new System.Drawing.Size(125, 20);
            this.txtNewId.TabIndex = 0;
            // 
            // txtNewGpa
            // 
            this.txtNewGpa.Location = new System.Drawing.Point(65, 54);
            this.txtNewGpa.Name = "txtNewGpa";
            this.txtNewGpa.Size = new System.Drawing.Size(125, 20);
            this.txtNewGpa.TabIndex = 1;
            // 
            // lblNewValues
            // 
            this.lblNewValues.AutoSize = true;
            this.lblNewValues.Location = new System.Drawing.Point(65, 9);
            this.lblNewValues.Name = "lblNewValues";
            this.lblNewValues.Size = new System.Drawing.Size(64, 13);
            this.lblNewValues.TabIndex = 2;
            this.lblNewValues.Text = "New Values";
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(30, 31);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(18, 13);
            this.lblId.TabIndex = 3;
            this.lblId.Text = "ID";
            // 
            // lblGpa
            // 
            this.lblGpa.AutoSize = true;
            this.lblGpa.Location = new System.Drawing.Point(30, 57);
            this.lblGpa.Name = "lblGpa";
            this.lblGpa.Size = new System.Drawing.Size(29, 13);
            this.lblGpa.TabIndex = 4;
            this.lblGpa.Text = "GPA";
            // 
            // lblCurrentValues
            // 
            this.lblCurrentValues.AutoSize = true;
            this.lblCurrentValues.Location = new System.Drawing.Point(207, 9);
            this.lblCurrentValues.Name = "lblCurrentValues";
            this.lblCurrentValues.Size = new System.Drawing.Size(76, 13);
            this.lblCurrentValues.TabIndex = 5;
            this.lblCurrentValues.Text = "Current Values";
            // 
            // lblCurId
            // 
            this.lblCurId.AutoSize = true;
            this.lblCurId.Location = new System.Drawing.Point(207, 31);
            this.lblCurId.Name = "lblCurId";
            this.lblCurId.Size = new System.Drawing.Size(15, 13);
            this.lblCurId.TabIndex = 6;
            this.lblCurId.Text = "id";
            // 
            // lblCurGpa
            // 
            this.lblCurGpa.AutoSize = true;
            this.lblCurGpa.Location = new System.Drawing.Point(207, 57);
            this.lblCurGpa.Name = "lblCurGpa";
            this.lblCurGpa.Size = new System.Drawing.Size(25, 13);
            this.lblCurGpa.TabIndex = 7;
            this.lblCurGpa.Text = "gpa";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(65, 81);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 8;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(149, 84);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(41, 16);
            this.lblError.TabIndex = 9;
            this.lblError.Text = "error";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(536, 157);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.lblCurGpa);
            this.Controls.Add(this.lblCurId);
            this.Controls.Add(this.lblCurrentValues);
            this.Controls.Add(this.lblGpa);
            this.Controls.Add(this.lblId);
            this.Controls.Add(this.lblNewValues);
            this.Controls.Add(this.txtNewGpa);
            this.Controls.Add(this.txtNewId);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtNewId;
        private System.Windows.Forms.TextBox txtNewGpa;
        private System.Windows.Forms.Label lblNewValues;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.Label lblGpa;
        private System.Windows.Forms.Label lblCurrentValues;
        private System.Windows.Forms.Label lblCurId;
        private System.Windows.Forms.Label lblCurGpa;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Label lblError;
    }
}

