﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EX4
{
    public class Student
    {
        private int _id;
        private double _gpa;

        public Student(int id, double gpa)
        {
            _id = id;
            _gpa = gpa;
        }

        public event ChangedEventHandler Changed;

        public int Id
        {
            get { return _id; }
            set
            {
                if (_id != value)
                {
                    _id = value;
                    OnChanged(EventArgs.Empty);
                }
            }
        }

        public double Gpa
        {
            get { return _gpa; }
            set
            {
                if (_gpa != value)
                {
                    _gpa = value;
                    OnChanged(EventArgs.Empty);
                }
            }
        }

        private void OnChanged(EventArgs e)
        {
            if (Changed != null)
            {
                Changed(this, e);
            }
        }
    }
}
