﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EX4
{
    public partial class Form1 : Form
    {
        private Student _student;

        public Form1()
        {
            InitializeComponent();

            _student = new Student(1, 4.0);
            _student.Changed += StudentChanged;

            txtNewId.Text = _student.Id.ToString();
            txtNewGpa.Text = _student.Gpa.ToString("0.0");
            lblCurId.Text = _student.Id.ToString();
            lblCurGpa.Text = _student.Gpa.ToString("0.0");
            lblError.Text = "";
        }

        private void StudentChanged(object sender, EventArgs e)
        {
            lblCurId.Text = _student.Id.ToString();
            lblCurGpa.Text = _student.Gpa.ToString("0.0");
            lblError.Text += "*UPDATED* ";
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            lblError.Text = "";
            try
            {
                int id = int.Parse(txtNewId.Text);
                double gpa = double.Parse(txtNewGpa.Text);

                _student.Id = id;
                _student.Gpa = gpa;
            }
            catch (FormatException)
            {
                lblError.Text = "Please enter valid values";
            }
        }
    }
}
