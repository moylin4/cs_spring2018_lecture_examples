﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace EX0
{
    public partial class PurchaseForm : Form
    {
        public PurchaseForm()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            // Parse and validate user input
            PaymentInfo info = new PaymentInfo();
            info.FullName = txtFullName.Text;
            info.CreditCardNumber = txtCreditCardNum.Text;
            info.ExpirationDate = new DateTime(
                int.Parse(txtExpYear.Text),
                int.Parse(txtExpMonth.Text),
                1
            );
            info.CV = int.Parse(txtCV.Text);

            // Save file
            var dlg = new SaveFileDialog();
            dlg.Filter =
                "JSON File (*.json)|*.json|" +
                "CSV File (*.csv)|*.csv|" +
                "All Files (*.*)|*.*";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                if (SaveFile(dlg.FileName, info))
                {
                    ShowSuccessMessage("File Saved");
                }
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            var dlg = new OpenFileDialog();
            dlg.Filter = 
                "JSON File (*.json)|*.json|" +
                "CSV File (*.csv)|*.csv|" +
                "All Files (*.*)|*.*";
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                PaymentInfo info = LoadFile(dlg.FileName);
                if (info != null)
                {
                    txtFullName.Text = info.FullName;
                    txtCreditCardNum.Text = info.CreditCardNumber;
                    txtExpMonth.Text = info.ExpirationDate.Month.ToString();
                    txtExpYear.Text = info.ExpirationDate.Year.ToString();
                    txtCV.Text = info.CV.ToString();
                }
            }
        }

        private void ShowSuccessMessage(string message)
        {
            MessageBox.Show(
                message,
                message,
                MessageBoxButtons.OK,
                MessageBoxIcon.Information
            );
        }

        private void ShowErrorMessage(string message, Exception ex)
        {
            string text = message;
            if (ex != null)
            {
                text += $"\r\n{ex.GetType().Name}: {ex.Message}";
            }

            MessageBox.Show(
                text,
                message,
                MessageBoxButtons.OK,
                MessageBoxIcon.Error
            );
        }

        private bool SaveFile(string path, PaymentInfo info)
        {
            try
            {
                if (path.EndsWith(".json"))
                {
                    PaymentStorage.SaveJsonFile(path, info);
                    return true;
                }
                else if (path.EndsWith(".csv"))
                {
                    PaymentStorage.SaveCsvFile(path, info);
                    return true;
                }
                else
                {
                    throw new IOException($"Unsupported Extension: {path}");
                }
            }
            catch (IOException ex)
            {
                ShowErrorMessage("Failed to Save", ex);
                return false;
            }
        }

        private PaymentInfo LoadFile(string path)
        {
            try
            {
                if (!File.Exists(path))
                {
                    ShowErrorMessage($"File Not Found: {path}", null);
                    return null;
                }
                else if (path.EndsWith(".json"))
                {
                    return PaymentStorage.LoadJsonFile(path);
                }
                else if (path.EndsWith(".csv"))
                {
                    return PaymentStorage.LoadCsvFile(path);
                }
                else
                {
                    throw new IOException($"Unsupported Extension: {path}");
                }
            }
            catch (IOException ex)
            {
                ShowErrorMessage("Failed to Load", ex);
                return null;
            }
        }
    }
}
