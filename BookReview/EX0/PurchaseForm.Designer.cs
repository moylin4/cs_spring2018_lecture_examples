﻿namespace EX0
{
    partial class PurchaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtFullName = new System.Windows.Forms.TextBox();
            this.lblCreditCardNum = new System.Windows.Forms.Label();
            this.txtCreditCardNum = new System.Windows.Forms.MaskedTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lblExp = new System.Windows.Forms.Label();
            this.txtExpMonth = new System.Windows.Forms.TextBox();
            this.txtExpYear = new System.Windows.Forms.TextBox();
            this.lblExpSlash = new System.Windows.Forms.Label();
            this.lblCV = new System.Windows.Forms.Label();
            this.txtCV = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnLoad = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtFullName
            // 
            this.txtFullName.Location = new System.Drawing.Point(120, 41);
            this.txtFullName.Name = "txtFullName";
            this.txtFullName.Size = new System.Drawing.Size(272, 26);
            this.txtFullName.TabIndex = 0;
            // 
            // lblCreditCardNum
            // 
            this.lblCreditCardNum.AutoSize = true;
            this.lblCreditCardNum.Location = new System.Drawing.Point(12, 15);
            this.lblCreditCardNum.Name = "lblCreditCardNum";
            this.lblCreditCardNum.Size = new System.Drawing.Size(102, 20);
            this.lblCreditCardNum.TabIndex = 1;
            this.lblCreditCardNum.Text = "Credit Card #";
            // 
            // txtCreditCardNum
            // 
            this.txtCreditCardNum.Location = new System.Drawing.Point(120, 9);
            this.txtCreditCardNum.Name = "txtCreditCardNum";
            this.txtCreditCardNum.Size = new System.Drawing.Size(272, 26);
            this.txtCreditCardNum.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Full Name";
            // 
            // lblExp
            // 
            this.lblExp.AutoSize = true;
            this.lblExp.Location = new System.Drawing.Point(13, 73);
            this.lblExp.Name = "lblExp";
            this.lblExp.Size = new System.Drawing.Size(36, 20);
            this.lblExp.TabIndex = 4;
            this.lblExp.Text = "Exp";
            // 
            // txtExpMonth
            // 
            this.txtExpMonth.Location = new System.Drawing.Point(120, 70);
            this.txtExpMonth.Name = "txtExpMonth";
            this.txtExpMonth.Size = new System.Drawing.Size(49, 26);
            this.txtExpMonth.TabIndex = 5;
            // 
            // txtExpYear
            // 
            this.txtExpYear.Location = new System.Drawing.Point(194, 67);
            this.txtExpYear.Name = "txtExpYear";
            this.txtExpYear.Size = new System.Drawing.Size(49, 26);
            this.txtExpYear.TabIndex = 6;
            // 
            // lblExpSlash
            // 
            this.lblExpSlash.AutoSize = true;
            this.lblExpSlash.Location = new System.Drawing.Point(175, 73);
            this.lblExpSlash.Name = "lblExpSlash";
            this.lblExpSlash.Size = new System.Drawing.Size(13, 20);
            this.lblExpSlash.TabIndex = 7;
            this.lblExpSlash.Text = "/";
            // 
            // lblCV
            // 
            this.lblCV.AutoSize = true;
            this.lblCV.Location = new System.Drawing.Point(249, 76);
            this.lblCV.Name = "lblCV";
            this.lblCV.Size = new System.Drawing.Size(73, 20);
            this.lblCV.TabIndex = 8;
            this.lblCV.Text = "CV Code";
            // 
            // txtCV
            // 
            this.txtCV.Location = new System.Drawing.Point(328, 73);
            this.txtCV.Name = "txtCV";
            this.txtCV.Size = new System.Drawing.Size(64, 26);
            this.txtCV.TabIndex = 9;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(16, 116);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(107, 48);
            this.btnSave.TabIndex = 10;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnLoad
            // 
            this.btnLoad.Location = new System.Drawing.Point(144, 116);
            this.btnLoad.Name = "btnLoad";
            this.btnLoad.Size = new System.Drawing.Size(99, 48);
            this.btnLoad.TabIndex = 11;
            this.btnLoad.Text = "Load";
            this.btnLoad.UseVisualStyleBackColor = true;
            this.btnLoad.Click += new System.EventHandler(this.btnLoad_Click);
            // 
            // PurchaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(460, 472);
            this.Controls.Add(this.btnLoad);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.txtCV);
            this.Controls.Add(this.lblCV);
            this.Controls.Add(this.lblExpSlash);
            this.Controls.Add(this.txtExpYear);
            this.Controls.Add(this.txtExpMonth);
            this.Controls.Add(this.lblExp);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtCreditCardNum);
            this.Controls.Add(this.lblCreditCardNum);
            this.Controls.Add(this.txtFullName);
            this.Name = "PurchaseForm";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtFullName;
        private System.Windows.Forms.Label lblCreditCardNum;
        private System.Windows.Forms.MaskedTextBox txtCreditCardNum;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblExp;
        private System.Windows.Forms.TextBox txtExpMonth;
        private System.Windows.Forms.TextBox txtExpYear;
        private System.Windows.Forms.Label lblExpSlash;
        private System.Windows.Forms.Label lblCV;
        private System.Windows.Forms.TextBox txtCV;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnLoad;
    }
}

