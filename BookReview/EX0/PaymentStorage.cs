﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace EX0
{
    public static class PaymentStorage
    {
        public const char CSV_DELIM = ';';

        public static bool SaveJsonFile(string path, PaymentInfo info)
        {
            using (var stream = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                using (var writer = new StreamWriter(stream))
                {
                    var s = new JavaScriptSerializer();
                    string json = s.Serialize(info);

                    writer.WriteLine(json);
                    writer.Flush();
                    return true;
                }
            }
        }

        public static PaymentInfo LoadJsonFile(string path)
        {
            using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                using (var reader = new StreamReader(stream))
                {
                    var s = new JavaScriptSerializer();
                    string json = reader.ReadToEnd();
                    PaymentInfo info = s.Deserialize<PaymentInfo>(json);

                    return info;
                }
            }
        }

        public static bool SaveCsvFile(string path, PaymentInfo info)
        {
            StringBuilder sb = new StringBuilder();
            using (var stream = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                using (var writer = new StreamWriter(stream))
                {
                    sb.Clear();
                    sb.Append(info.FullName);
                    sb.Append(CSV_DELIM);
                    sb.Append(info.CreditCardNumber);
                    sb.Append(CSV_DELIM);
                    sb.Append(info.ExpirationDate.ToString(
                        "MM/yyyy", CultureInfo.InvariantCulture
                    ));
                    sb.Append(CSV_DELIM);
                    sb.Append(info.CV);
                    sb.Append(CSV_DELIM);

                    writer.WriteLine(sb.ToString());
                    writer.Flush();
                    return true;
                }
            }
        }

        public static PaymentInfo LoadCsvFile(string path)
        {
            using (var stream = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                using (var reader = new StreamReader(stream))
                {
                    PaymentInfo info = new PaymentInfo();
                    string line = reader.ReadLine();
                    string[] fields = line.Split(CSV_DELIM);

                    info.FullName = fields[0];
                    info.CreditCardNumber = fields[1];
                    info.ExpirationDate = DateTime.ParseExact(
                        fields[2], "MM/yyyy", CultureInfo.InvariantCulture
                    );
                    info.CV = int.Parse(fields[3]);

                    return info;
                }
            }
        }
    }
}
